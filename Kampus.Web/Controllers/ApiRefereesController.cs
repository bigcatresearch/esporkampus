﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kampus.DAL;

namespace Kampus.Web.Controllers
{
    public class ApiRefereesController : ApiController
    {
        private MyModel db = new MyModel();

        // GET: api/ApiReferees
        public IQueryable<Referees> GetReferees()
        {
            return db.Referees;
        }

        // GET: api/ApiReferees/5
        [ResponseType(typeof(Referees))]
        public IHttpActionResult GetReferees(int id)
        {
            Referees referees = db.Referees.Find(id);
            if (referees == null)
            {
                return NotFound();
            }

            return Ok(referees);
        }

        // PUT: api/ApiReferees/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReferees(int id, Referees referees)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != referees.RefereeId)
            {
                return BadRequest();
            }

            db.Entry(referees).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RefereesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiReferees
        [ResponseType(typeof(Referees))]
        public IHttpActionResult PostReferees(Referees referees)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Referees.Add(referees);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = referees.RefereeId }, referees);
        }

        // DELETE: api/ApiReferees/5
        [ResponseType(typeof(Referees))]
        public IHttpActionResult DeleteReferees(int id)
        {
            Referees referees = db.Referees.Find(id);
            if (referees == null)
            {
                return NotFound();
            }

            db.Referees.Remove(referees);
            db.SaveChanges();

            return Ok(referees);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RefereesExists(int id)
        {
            return db.Referees.Count(e => e.RefereeId == id) > 0;
        }
    }
}