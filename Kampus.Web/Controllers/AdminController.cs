﻿using Kampus.Business;
using Kampus.DAL;
using Kampus.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kampus.Web.Controllers
{
    public class AdminController : Controller
    {
        UserRepository userRepository = new UserRepository();
        MessageRepository messageRepository = new MessageRepository();
        GameRepository gameRepository = new GameRepository();
        BlogRepository blogRepository = new BlogRepository();
        ImageUpload imageUpload = new ImageUpload();

        // GET: Index
        #region

        public ActionResult Index()
        {
            return View();
        }
        #endregion

        // BLOG
        #region

        // GET: Blog
        #region
        public ActionResult Blog()
        {
            /*  MyModel db = new MyModel();
              return View(db.Blogs.ToList());*/
            return View(blogRepository.GetAllBlogs());
        }
        #endregion
        //GET: BlogEdit
        #region
        public ActionResult BlogEdit(int id)
        {
            MyModel db = new MyModel();
            ViewBag.GameList = db.Games.ToList();
            var blog = db.Blogs.FirstOrDefault(x => x.BlogId == id);
            return View(blog);
        }
        #endregion
        //POST: BlogEdit
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult BlogEdit([Bind(Include = "BlogId,WriterId,GameId,IsActive,BlogTitle,BlogImageURL,BlogContent,CreatedDate,Priority")] Blogs blog, HttpPostedFileBase BlogResmi, int OyunId)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.GameList = db.Games.ToList();
                if (blog.BlogTitle == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                if (blog.Priority == true)
                {
                    var oncelikliBlog = db.Blogs.FirstOrDefault(x => x.Priority == true);
                    oncelikliBlog.Priority = false;

                    db.SaveChanges();
                }
                else { blog.Priority = false; }

                var br = db.Blogs.FirstOrDefault(x => x.BlogId == blog.BlogId);
                br.GameId = OyunId;
                br.Priority = blog.Priority;
                br.BlogTitle = blog.BlogTitle;
                br.BlogContent = blog.BlogContent;
                br.BlogImageURL = br.BlogImageURL;
                br.IsActive = br.IsActive;
                if (BlogResmi != null)
                { br.BlogImageURL = imageUpload.ImageResize(BlogResmi, 500, 420); }

                blogRepository.EditBlog(br, blog.BlogId);
                return RedirectToAction("Blog");
            }
            return RedirectToAction("Blog");
        }
        #endregion
        // GET: BlogAdd
        #region
        public ActionResult BlogAdd()
        {
            MyModel db = new MyModel();
            ViewBag.GameList = db.Games.ToList();
            return View();
        }
        #endregion
        //POST: BlogAdd
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult BlogAdd([Bind(Include = "BlogId,WriterId,GameId,IsActive,BlogTitle,BlogImageURL,BlogContent,CreatedDate,Priority")] Blogs blog, HttpPostedFileBase BlogResmi, int OyunId)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.GamesList = db.Games.ToList();

                if (blog.BlogTitle == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                if (blog.Priority == true)
                {
                    var oncelikliBlog = db.Blogs.FirstOrDefault(x => x.Priority == true);
                    oncelikliBlog.Priority = false;

                    db.SaveChanges();
                }
                else { blog.Priority = false; }
                blog.GameId = OyunId;
                blog.BlogImageURL = imageUpload.ImageResize(BlogResmi, 500, 420);
                blog.IsActive = true;
                blog.CreatedDate = DateTime.Now;
                blogRepository.CreateBlog(blog);
                return RedirectToAction("Blog");
            }
            return RedirectToAction("Blog");
        }
        #endregion
        //DELETE: Blogs
        #region
        public ActionResult BlogDelete(int id)
        {
            blogRepository.DeleteBlog(id);
            return Redirect("/Admin/Blog/");
        }
        #endregion
        //DURUM: Blogs
        #region
        public ActionResult BlogDurum(int id)
        {
            MyModel db = new MyModel();
            var br = db.Blogs.FirstOrDefault(x => x.BlogId == id);
            if (br.Priority == false)
            {
                if (br.IsActive == true)
                {
                    br.IsActive = false;
                }
                else
                {
                    br.IsActive = true;
                }
                db.SaveChanges();
            }

            return RedirectToAction("Blog");
        }
        #endregion
        //GET: BlogDetail
        #region
        public ActionResult BlogDetail(int id)
        {
            MyModel db = new MyModel();
            var br = db.Blogs.FirstOrDefault(x => x.BlogId == id);
            return View(br);
        }
        #endregion
        #endregion

        // USERS
        #region

        // GET: Users
        #region
        public ActionResult Users()
        {
            return View(userRepository.GetAllUsers().Where(x => x.Role != 4));
        }
        #endregion
        // GET: UserAdd
        #region
        public ActionResult UserAdd()
        {
            MyModel db = new MyModel();
            ViewBag.PageList = db.Universities.ToList();
            return View();
        }
        #endregion
        //POST: UserAdd
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserAdd([Bind(Include = "UserId,UserName,NickName,Email,Password,Role,UniversityId")] Users user, string Rol)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.PageList = db.Universities.ToList();

                if (user.UserName == null || user.Email == null || Rol == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                switch (Rol)
                {
                    case "Sistem":
                        user.Role = 0;
                        break;
                    case "Editör":
                        user.Role = 1;
                        break;
                    case "İstatistik":
                        user.Role = 2;
                        break;
                }
                user.IsActive = true;
                user.CreatedDate = DateTime.Now;
                user.Wins = 0;
                user.Ties = 0;
                user.Loses = 0;
                userRepository.CreateUser(user);
                return Redirect("/Admin/Users/");
            }
            return Redirect("/Admin/Users/");
        }
        #endregion

        //GET: UserDetail
        #region
        public ActionResult UserDetail(int? id)
        {
            return View(userRepository.GetUserById(id));
        }
        #endregion
        // GET: UserEdit 
        #region 
        public ActionResult UserEdit(int id)
        {
            MyModel db = new MyModel();

            ViewBag.PageList = db.Universities.ToList();
            var br = db.Users.FirstOrDefault(a => a.UserId == id);
            return View(br);
        }

        #endregion
        // POST :UserEdit
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserEdit([Bind(Include = "UserId,UserName,NickName,Email,Password,Role,UniversityId")] int id, string Rol, Users user)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.PageList = db.Universities.ToList();
                if (user.UserName == null || user.Email == null || Rol == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                switch (Rol)
                {
                    case "Sistem":
                        user.Role = 0;
                        break;
                    case "Editör":
                        user.Role = 1;
                        break;
                    case "İstatistik":
                        user.Role = 2;
                        break;
                }

                var br = db.Users.FirstOrDefault(a => a.UserId == id);
                br.GameId = 0;
                br.TeamId = 0;
                br.UserId = user.UserId;
                br.Role = user.Role;
                user.IsActive = br.IsActive;
                br.UserName = user.UserName;
                br.Password = user.Password;

                br.Email = user.Email;
                br.UniversityId = user.UniversityId;
                userRepository.EditUser(br, id);
                return Redirect("/Admin/Users/");
            }
            return Redirect("/Admin/Users/");
        }
        #endregion

        //DELETE: Users
        #region
        public ActionResult UserDelete(int id)
        {
            userRepository.DeleteUser(id);
            return Redirect("/Admin/Users/");
        }
        #endregion

        //DURUM : Users
        #region
        public ActionResult UserDurum(int id)
        {
            MyModel db = new MyModel();
            var br = db.Users.FirstOrDefault(a => a.UserId == id);
            if (br.IsActive == true)
            {
                br.IsActive = false;
            }
            else
            {
                br.IsActive = true;
            }
            userRepository.EditUser(br, id);
            return Redirect("/Admin/Users/");
        }
        #endregion
        #endregion

        // GET:Gamers
        #region
        public ActionResult Gamers()
        {
            return View(userRepository.GetAllUsers().Where(x => x.Role == 4));
        }
        #endregion
        //GET: GamerDetail
        #region
        public ActionResult GamerDetail(int? id)
        {
            return View(userRepository.GetUserById(id));
        }
        #endregion

        // MESSAGES
        #region
        // GET: Messages
        #region
        public ActionResult Messages()
        {
            return View(messageRepository.GetAllMessages());
        }
        #endregion
        // GET: MessageDetail
        #region
        public ActionResult MessageDetail(int id)
        {
            MessageDurum(id);
            return View(messageRepository.GetMessageById(id));
        }
        #endregion
        // DURUM: Messages
        #region
        public void MessageDurum(int id)
        {
            MyModel db = new MyModel();
            var br = db.Messages.FirstOrDefault(x => x.MessageId == id);
            if (br.IsRead == false)
            {
                br.IsRead = true;
                br.RowBgColor = "";
                db.SaveChanges();
            }
        }
        #endregion
        // DELETE : Messages
        #region
        public ActionResult MessageDelete(int id)
        {
            messageRepository.DeleteMessage(id);
            return Redirect("/Admin/Messages/");
        }
        #endregion
        #endregion


        // GET: Settings
        public ActionResult Settings()
        {
            return View();
        }

        // GAMES
        #region
        // GET: Games
        #region
        public ActionResult Games()
        {
            /* MyModel db = new MyModel();
             return View(db.Games.ToList());*/
            return View(gameRepository.GetAllGames());
        }
        #endregion
        // GET: GameAdd
        #region
        public ActionResult GameAdd()
        {
            return View();
        }
        #endregion
        //POST: GameAdd
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult GameAdd([Bind(Include = "GameId,GameName,GameLogoURL,IsActive,GameDescription,CreatedDate,UserCount, GameRules,FirstPrize,SecondPrize,ThirdPrize,GameCategory,GameDescriptionExtra")] Games game, HttpPostedFileBase GameImage)
        {
            if (ModelState.IsValid)
            {

                if (game.GameName == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                game.GameLogoURL = imageUpload.ImageResize(GameImage, 500, 420);
                game.IsActive = true;
                game.CreatedDate = DateTime.Now;
                game.UserCount = 0;
                gameRepository.CreateGame(game);
                return RedirectToAction("Games");
            }
            return RedirectToAction("Games");
        }
        #endregion

        //GET: GameDetail
        #region
        public ActionResult GameDetail(int? id)
        {
            return View(gameRepository.GetGameById(id));
        }
        #endregion
        // GET: GameEdit 
        #region 
        public ActionResult GameEdit(int id)
        {
            MyModel db = new MyModel();
            var br = db.Games.FirstOrDefault(a => a.GameId == id);
            return View(br);
        }

        #endregion
        // POST :GameEdit
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult GameEdit([Bind(Include = "GameId,GameName,GameLogoURL,IsActive,GameDescription,CreatedDate,UserCount,GameRules,FirstPrize,SecondPrize,ThirdPrize,GameCategory,GameDescriptionExtra")] int id, Games game)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                if (game.GameName == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }

                var br = db.Games.FirstOrDefault(a => a.GameId == id);
                br.GameCategory = game.GameCategory;
                br.GameRules = game.GameRules;
                game.IsActive = br.IsActive;
                db.SaveChanges();
                return RedirectToAction("Games");
            }
            return RedirectToAction("Games");
        }
        #endregion

        //DELETE: Games
        #region
        public ActionResult GameDelete(int id)
        {
            gameRepository.DeleteGame(id);
            return Redirect("/Admin/Games/");
        }
        #endregion

        //DURUM : Games
        #region
        public ActionResult GameDurum(int id)
        {
            MyModel db = new MyModel();
            var br = db.Games.FirstOrDefault(a => a.GameId == id);
            if (br.IsActive == true)
            {
                br.IsActive = false;
            }
            else
            {
                br.IsActive = true;
            }
            db.SaveChanges();
            return RedirectToAction("Games");
        }
        #endregion
        #endregion

        // ANA SAYFA Dinamik Ogeleri
        #region
        //GET:Intro
        #region
        public ActionResult Intro()
        {
            MyModel db = new MyModel();
            var intro = db.Intro.FirstOrDefault(x => x.IntroId == 1);
            return View(intro);
        }
        #endregion

        //POST:Intro
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Intro([Bind(Include = "IntroId,Header,Button,Background")] Intro intro)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {
                var br = db.Intro.FirstOrDefault(x => x.IntroId == 1);
                br.Header = intro.Header;
                br.ImageUrl = intro.ImageUrl;
                br.Button = intro.Button;
                db.SaveChanges();
                return Redirect("/Admin/Intro/");
            }
            return Redirect("/Admin/Intro/");
        }
        #endregion

        //GET:About
        #region
        public ActionResult About()
        {
            MyModel db = new MyModel();
            var about = db.About.FirstOrDefault(x => x.AboutId == 1);
            return View(about);
        }
        #endregion

        //POST:About
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult About([Bind(Include = "AboutId,AboutUs,Text1Header,Text1Body,Text2Header,Text2Body,Button_text,Button_link")] About about)
        {
            MyModel db = new MyModel();
            var br = db.About.FirstOrDefault(x => x.AboutId == 1);
            br.AboutUs = about.AboutUs;
            br.Button_link = about.Button_link;
            br.Button_text = about.Button_text;
            br.Text1Header = about.Text1Header;
            br.Text1Body = about.Text1Body;
            br.Text2Header = about.Text2Header;
            br.Text2Body = about.Text2Body;

            db.SaveChanges();
            return Redirect("/Admin/About/");
        }
        #endregion

        //GET:Terms
        #region
        public ActionResult Terms()
        {
            MyModel db = new MyModel();
            var task = db.TaskOfJoin.FirstOrDefault(x => x.TaskOfJoinId == 1);
            return View(task);
        }
        #endregion

        //POST:Terms
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Terms([Bind(Include = "TaskOfJoinId,Header,Rule1,Rule2,Rule3,Rule4,Rule5,Rule6")] TaskOfJoin task)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {
                var br = db.TaskOfJoin.FirstOrDefault(x => x.TaskOfJoinId == 1);
                br.Header = task.Header;
                br.Rule1 = task.Rule1;
                br.Rule2 = task.Rule2;
                br.Rule3 = task.Rule3;
                br.Rule4 = task.Rule4;
                br.Rule5 = task.Rule5;
                br.Rule6 = task.Rule6;

                db.SaveChanges();
                return Redirect("/Admin/Terms/");
            }
            return Redirect("/Admin/Terms/");
        }
        #endregion

        //GET:GameCalendar
        #region
        public ActionResult GameCalendar()
        {
            MyModel db = new MyModel();
            return View(db.CalendarOfGames.ToList());
        }
        #endregion

        //GET:GameCalendarDetail
        #region
        public ActionResult GameCalendarDetail(int id)
        {
            MyModel db = new MyModel();
            ViewBag.GameList = db.Games.ToList();
            var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
            return View(br);
        }
        #endregion

        //GET:GameCalendarAdd
        #region
        public ActionResult GameCalendarAdd()
        {
            MyModel db = new MyModel();
            ViewBag.GameList = db.Games.ToList();
            return View();
        }
        #endregion

        //POST:GameCalendarAdd
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GameCalendarAdd([Bind(Include = "CalendarOfGameId,GameId,IsActive,Date1,Date2,Date3,Date4,Date5,Date6")] CalendarOfGames calendar)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {
                calendar.IsActive = true;
                db.CalendarOfGames.Add(calendar);
                db.SaveChanges();
                return Redirect("/Admin/GameCalendar/");
            }
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion

        //GET:GameCalendarEdit
        #region
        public ActionResult GameCalendarEdit(int id)
        {
            MyModel db = new MyModel();
            ViewBag.GameList = db.Games.ToList();
            var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
            return View(br);
        }
        #endregion

        //POST:GameCalendarEdit
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GameCalendarEdit([Bind(Include = "CalendarOfGameId,GameId,IsActive,Date1,Date2,Date3,Date4,Date5,Date6")] CalendarOfGames calendar)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {
                var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == calendar.CalendarOfGameId);
                br.GameId = calendar.GameId;
                br.Date1 = calendar.Date1;
                br.Date2 = calendar.Date2;
                br.Date3 = calendar.Date3;
                br.Date4 = calendar.Date4;
                br.Date5 = calendar.Date5;
                br.Date6 = calendar.Date6;

                db.SaveChanges();
                return Redirect("/Admin/GameCalendar/");
            }
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion

        //DURUM:GameCalendarDurum
        #region
        public ActionResult GameCalendarDurum(int id)
        {
            MyModel db = new MyModel();
            var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
            if (br.IsActive == true)
            {
                br.IsActive = false;
            }
            else
            {
                br.IsActive = true;
            }
            db.SaveChanges();
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion

        //DELETE: GameCalendar
        #region
        public ActionResult GameCalendarDelete(int id)
        {
            MyModel db = new MyModel();
            var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
            db.CalendarOfGames.Remove(br);
            db.SaveChanges();
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion
        #endregion

    }
}