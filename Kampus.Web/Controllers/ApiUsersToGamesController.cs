﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kampus.DAL;

namespace Kampus.Web.Controllers
{
    public class ApiUsersToGamesController : ApiController
    {
        private MyModel db = new MyModel();

        // GET: api/ApiUsersToGames
        public IQueryable<UsersToGames> GetUsersToGames()
        {
            return db.UsersToGames;
        }

        // GET: api/ApiUsersToGames/5
        [ResponseType(typeof(UsersToGames))]
        public IHttpActionResult GetUsersToGames(int id)
        {
            UsersToGames usersToGames = db.UsersToGames.Find(id);
            if (usersToGames == null)
            {
                return NotFound();
            }

            return Ok(usersToGames);
        }

        // PUT: api/ApiUsersToGames/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUsersToGames(int id, UsersToGames usersToGames)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usersToGames.UTG_Id)
            {
                return BadRequest();
            }

            db.Entry(usersToGames).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersToGamesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiUsersToGames
        [ResponseType(typeof(UsersToGames))]
        public IHttpActionResult PostUsersToGames(UsersToGames usersToGames)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UsersToGames.Add(usersToGames);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = usersToGames.UTG_Id }, usersToGames);
        }

        // DELETE: api/ApiUsersToGames/5
        [ResponseType(typeof(UsersToGames))]
        public IHttpActionResult DeleteUsersToGames(int id)
        {
            UsersToGames usersToGames = db.UsersToGames.Find(id);
            if (usersToGames == null)
            {
                return NotFound();
            }

            db.UsersToGames.Remove(usersToGames);
            db.SaveChanges();

            return Ok(usersToGames);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UsersToGamesExists(int id)
        {
            return db.UsersToGames.Count(e => e.UTG_Id == id) > 0;
        }
    }
}