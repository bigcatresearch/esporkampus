﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kampus.DAL;

namespace Kampus.Web.Controllers
{
    public class ApiCharactersController : ApiController
    {
        private MyModel db = new MyModel();

        // GET: api/ApiCharacters
        public IQueryable<Characters> GetCharacters()
        {
            return db.Characters;
        }

        // GET: api/ApiCharacters/5
        [ResponseType(typeof(Characters))]
        public IHttpActionResult GetCharacters(int id)
        {
            Characters characters = db.Characters.Find(id);
            if (characters == null)
            {
                return NotFound();
            }

            return Ok(characters);
        }

        // PUT: api/ApiCharacters/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCharacters(int id, Characters characters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != characters.CharacterId)
            {
                return BadRequest();
            }

            db.Entry(characters).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharactersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiCharacters
        [ResponseType(typeof(Characters))]
        public IHttpActionResult PostCharacters(Characters characters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Characters.Add(characters);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = characters.CharacterId }, characters);
        }

        // DELETE: api/ApiCharacters/5
        [ResponseType(typeof(Characters))]
        public IHttpActionResult DeleteCharacters(int id)
        {
            Characters characters = db.Characters.Find(id);
            if (characters == null)
            {
                return NotFound();
            }

            db.Characters.Remove(characters);
            db.SaveChanges();

            return Ok(characters);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CharactersExists(int id)
        {
            return db.Characters.Count(e => e.CharacterId == id) > 0;
        }
    }
}