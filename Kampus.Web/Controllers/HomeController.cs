﻿using Kampus.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Kampus.Business;
using System.Web.Script.Serialization;
using Kampus.Web.Models;

namespace Kampus.Web.Controllers
{
    public class HomeController : Controller
    {
        ImageUpload imageUpload = new ImageUpload();
        UserRepository userRepository = new UserRepository();
        MessageRepository messageRepository = new MessageRepository();
        TeamRepository teamRepository = new TeamRepository();
        UsersToGamesRepository usersToGamesRepository = new UsersToGamesRepository();


        public ActionResult Index()
        {
            MyModel db = new MyModel();

            ViewBag.Calendar = db.CalendarOfGames.Where(x => x.IsActive == true);

            var intro = db.Intro.FirstOrDefault(x => x.IntroId == 1);
            ViewBag.IntroBaslik = intro.Header;
            ViewBag.ButtonLink = intro.Button;

            var about = db.About.FirstOrDefault(x => x.AboutId == 1);
            ViewBag.BizKimiz = about.AboutUs;
            ViewBag.Text1Baslik = about.Text1Header;
            ViewBag.Text1Icerik = about.Text1Body;
            ViewBag.Text2Baslik = about.Text2Header;
            ViewBag.Text2Icerik = about.Text2Body;

            var terms = db.TaskOfJoin.FirstOrDefault(x => x.TaskOfJoinId == 1);
            ViewBag.Header = terms.Header;
            ViewBag.Rule1 = terms.Rule1;
            ViewBag.Rule2 = terms.Rule2;
            ViewBag.Rule3 = terms.Rule3;
            ViewBag.Rule4 = terms.Rule4;
            ViewBag.Rule5 = terms.Rule5;
            ViewBag.Rule6 = terms.Rule6;


            var OncelikliBlog = db.Blogs.FirstOrDefault(x => x.Priority == true);
            ViewBag.BlogBaslik = OncelikliBlog.BlogTitle;
            ViewBag.BlogId = OncelikliBlog.BlogId;
            if (OncelikliBlog.BlogContent.Count() > 250)
            { ViewBag.BlogIcerik = OncelikliBlog.BlogContent.Substring(0, 250); }
            else { ViewBag.BlogIcerik = OncelikliBlog.BlogContent; }
            ViewBag.BlogTarih = OncelikliBlog.CreatedDate;
            ViewBag.BlogResim = OncelikliBlog.BlogImageURL;
            return View();
        }

        public ActionResult Blogs()
        {
            MyModel db = new MyModel();
            return View(db.Blogs.Where(x => x.IsActive == true).ToList());
        }

        public ActionResult BlogDetail(int id)
        {
            MyModel db = new MyModel();
            var br = db.Blogs.FirstOrDefault(x => x.BlogId == id);
            return View(br);
        }

        // TAKIM BAŞVURUSU
        #region
        public ActionResult Team(int id)
        {
            MyModel db = new MyModel();
            if (Session["KullaniciId"] != null)
            {
                ViewBag.OyunId = id.ToString();
                ViewBag.UniversityList = db.Universities.ToList();
                ViewBag.GamesList = db.Games.ToList();
                ViewBag.TeamList = db.Teams.ToList();
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        // TAKIM EKLEME KISMI
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Team([Bind(Include = "TeamId,UserId,MatchId,TeamDetailId,UniversityId,GamesId,TeamName,About,LogoImageURL,TeamCount,IsActive,CreatedDate,Wins,Loses,Ties,GameId")] Teams team, int OyunId, HttpPostedFileBase TeamLogo)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {


                string kId = Session["KullaniciId"].ToString();
                int kidd = Convert.ToInt32(kId);
                var kullanici = db.Users.FirstOrDefault(x => x.UserId == kidd);

                ViewBag.UniversityList = db.Universities.ToList();
                ViewBag.GamesList = db.Games.ToList();

                team.LogoImageURL = imageUpload.ImageResize(TeamLogo, 500, 420);
                team.GameId = OyunId;
                team.IsActive = true;
                team.CreatedDate = DateTime.Now;
                team.GamerWish = false;
                team.Wins = 0;
                team.Ties = 0;
                team.Loses = 0;
                teamRepository.CreateTeam(team);

                db.UsersToTeams.Add(new UsersToTeams
                {
                    UserId = kullanici.UserId,
                    TeamId = db.Teams.Max(x => x.TeamId),
                });
                db.SaveChanges();
                db.TeamsToGames.Add(new TeamsToGames
                {
                    TeamId = db.Teams.Max(x => x.TeamId),
                    GameId = OyunId
                });
                db.SaveChanges();
                return Redirect("/Home/TeamPage/" + db.Teams.Max(x => x.TeamId));
            }
            return Redirect("/Home/TeamPage/" + db.Teams.Max(x => x.TeamId));
        }
        #endregion

        // BİREYSEL BAŞVURU
        #region


        public ActionResult Single(int id)
        {
            MyModel db = new MyModel();
            ViewBag.OyunId = id.ToString();

            if (Session["KullaniciId"] != null)
            {
                ViewBag.UniversityList = db.Universities.ToList();
                ViewBag.GamesList = db.Games.ToList();
                ViewBag.TeamList = db.Teams.ToList();
                return View();
            }
            else
            {
                Session["BasvuruSayfasi"] = id;
                return RedirectToAction("Login");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Single(string RoleinGame, string witchName, string UserRank, string UserServer, string StreamProfile, bool haveAteam, string oyunId)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.UniversiteList = db.Universities.ToList();
                ViewBag.OyunList = db.Games.ToList();

                string kId = Session["KullaniciId"].ToString();
                int kidd = Convert.ToInt32(kId);

                UsersToGames utg = new UsersToGames();
                utg.GameId = Convert.ToInt32(oyunId);
                utg.UserId = kidd;
                utg.WitchName = witchName;
                utg.StreamProfile =
                utg.UserServer = UserServer;
                utg.RoleInGame = RoleinGame;
                utg.HaveTeam = haveAteam;
                utg.TeamWish = false;
                utg.UserRank = UserRank;

                db.UsersToGames.Add(utg);
                db.SaveChanges();
                return Redirect("/Home/Games/" + oyunId);
            }
            return Redirect("/Home/Games/" + oyunId);
        }

        #endregion

        // UNİVERSİTE 
        #region

        public ActionResult Universities()
        {
            MyModel db = new MyModel();
            return View(db.Universities.ToList());
        }

        public ActionResult UniversityDetail(int? id)
        {
            if (id != null)
            {
                MyModel db = new MyModel();
                var uni = db.Universities.FirstOrDefault(x => x.UniversityId == id);
                ViewBag.Teams = db.Teams.Where(x => x.UniversityId == id).Where(x => x.IsActive == true).ToList();

                var usersWhoWishTeam = db.TeamAdvertisements.Where(x => x.Users.IsActive == true).Where(x => x.Users.UniversityId == id);
                ViewBag.usersWhoWishTeam = usersWhoWishTeam.ToList();
                var teamsWhoWishGamer = db.GamerAdvertisements.Where(x => x.Teams.IsActive == true).Where(x => x.Teams.UniversityId == id);
                ViewBag.teamsWhoWishGamer = teamsWhoWishGamer.ToList();

                ViewBag.GameAds = db.GamerAdvertisements.ToList();
                ViewBag.TeamAds = db.TeamAdvertisements.ToList();

                if (Session["KullaniciId"] != null)
                {
                    string kId = Session["KullaniciId"].ToString();
                    int kidd = Convert.ToInt32(kId);
                    ViewBag.KullaniciID = kidd;

                    ViewBag.GamesList = db.Games.ToList();
                    ViewBag.TeamList = db.UsersToTeams.Where(x => x.UserId == kidd).ToList();
                }
                return View(uni);
            }
            else
            {
                return RedirectToAction("Universities");
            }

        }

        [HttpPost]
        public ActionResult GamerAdvertisementAdd(int teamId, int gameId, string gamerExperience, string gamerDescription, string DiscordAdress)
        {
            MyModel db = new MyModel();
            db.GamerAdvertisements.Add(new GamerAdvertisements
            {
                TeamId = teamId,
                GameId = gameId,
                DiscordURL = DiscordAdress,
                GamerAdExprerience = gamerExperience,
                GamerAdDescription = gamerDescription
            });
            db.SaveChanges();
            return RedirectToAction("Universities");
        }

        [HttpPost]
        public ActionResult TeamAdvertisementAdd(int gameId, string turnuva1, string turnuva2, string turnuva3, string LolLigi, string rutbe, string streamProfili, string nick, string dicord, string Description)
        {
            MyModel db = new MyModel();
            string kId = Session["KullaniciId"].ToString();
            int kidd = Convert.ToInt32(kId);
            var kullanici = db.Users.FirstOrDefault(x => x.UserId == kidd);

            db.TeamAdvertisements.Add(new TeamAdvertisements
            {
                UserId = kullanici.UserId,
                GameId = gameId,
                Tournament1 = turnuva1,
                Tournament2 = turnuva2,
                Tournament3 = turnuva3,
                LolLeague = LolLigi,
                StreamProfile = streamProfili,
                UserRank = rutbe,
                NickName = nick,
                DiscordURL = dicord,
                Description = Description
            });
            db.SaveChanges();
            return RedirectToAction("Universities");
        }

        public ActionResult UniversityAdd()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UniversityAdd([Bind(Include = "UniversityId,UniversityName")]Universities Universite, string UniversiteAdi)
        {
            Universite.UniversityName = UniversiteAdi;
            Universite.UniversityScore = 0;
            using (HttpClient client = new HttpClient())
            {
                var data = JsonConvert.SerializeObject(Universite);
                HttpContent content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
                var returnResult = await client.PostAsync("http://esporkampus.com/api/ApiUniversities", content);
            }
            return View();
        }

        public ActionResult TeamWish(int id, int gameID)
        {
            MyModel db = new MyModel();
            var user = db.UsersToGames.Where(x => x.UserId == id).FirstOrDefault(x => x.GameId == gameID);
            user.TeamWish = true;
            db.SaveChanges();
            return Redirect("/Home/Universities/");
        }


        #endregion

        // POST: Message
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "MessageId,SenderName,SenderEmail,SenderIP,Subject,IsRead,IsUser,RowBgColor,SendingTime")] Messages message, string Mesaj, string AdSoyad, string Eposta, string Konu)
        {
            if (ModelState.IsValid)
            {

                if (AdSoyad == null || Eposta == null || Mesaj == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }

                var ipAddress = string.Empty;
                if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"] != null && System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"].Length != 0)
                {
                    ipAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"];
                }
                else if (System.Web.HttpContext.Current.Request.UserHostAddress.Length != 0)
                {
                    ipAddress = System.Web.HttpContext.Current.Request.UserHostName;
                }

                MyModel db = new MyModel();
                var IsUser = db.Users.FirstOrDefault(x => x.Email == Eposta);
                if (IsUser != null)
                {
                    message.IsUser = true;
                }
                else
                {
                    message.IsUser = false;
                }
                message.SenderEmail = Eposta;
                message.SenderName = AdSoyad;
                message.Subject = Konu;
                message.SenderIP = ipAddress.ToString();
                message.IsRead = false;
                message.SendingTime = DateTime.Now;
                message.RowBgColor = "#F5F5DC";
                message.MessageContent = Mesaj;
                messageRepository.CreateMessage(message);
            }
            return Redirect("/Home/Index/");
        }
        #endregion

        public ActionResult TeamPage(int id)
        {
            MyModel db = new MyModel();
            var team = db.Teams.FirstOrDefault(x => x.TeamId == id);

            if (Session["KullaniciId"] != null)
            {
                string kId = Session["KullaniciId"].ToString();
                int kidd = Convert.ToInt32(kId);
                var kullanici = db.Users.FirstOrDefault(x => x.UserId == kidd);

                var t = db.UsersToTeams.Where(x => x.TeamId == team.TeamId).FirstOrDefault(c => c.UserId == kullanici.UserId);

                if (t == null)
                {
                    ViewBag.ShowDiv = true;
                    Session["TakimaKatilacakKullanici"] = kullanici.UserId;
                }
                else
                {
                    ViewBag.ShowDiv = false;
                }
            }
            else
            {
                ViewBag.ShowDiv = true;
            }


            return View(team);
        }



        public ActionResult Games(int id)
        {
            MyModel db = new MyModel();

            var game = db.Games.FirstOrDefault(x => x.GameId == id);
            ViewBag.Teams = db.TeamsToGames.Where(x => x.GameId == id).ToList();
            ViewBag.Blogs = db.Blogs.Where(x => x.IsActive == true).Where(x => x.GameId == id).ToList();
            ViewBag.Match = db.Matches.Where(x => x.IsActive == true).Where(c => c.Games.GameId == id);
            return View(game);
        }

        // REGISTER
        #region

        public ActionResult Register()
        {
            MyModel db = new MyModel();
            ViewBag.UniversityList = db.Universities.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register([Bind(Include = "UserId,TeamId,GameId,MatchId,UniversityId,UserName,NickName,Email,Password,Role,BirthDate,Phone,Address,CreatedDate,IsActive,UserAbout,Wins,Loses,Ties,StreamProfile")] Users user)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {

                ViewBag.UniversiteList = db.Universities.ToList();
                ViewBag.OyunList = db.Games.ToList();
                ViewBag.TakımList = db.Teams.ToList();

                user.IsActive = true;
                user.CreatedDate = DateTime.Now;
                user.Ties = 0;
                user.Wins = 0;
                user.Loses = 0;
                user.Role = 4;
                if (user.UserName == null || user.Email == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }

                userRepository.CreateUser(user);
                return Redirect("/Home/Login/");
            }
            return Redirect("/Home/Login/");
        }
        #endregion
        // LOGIN
        #region

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string email, string password)
        {
            MyModel db = new MyModel();
            var kisi = db.Users.FirstOrDefault(x => x.Email == email);
            if (kisi != null)
            {
                if (kisi.Password == password && kisi.IsActive == true)
                {
                    Session["KullaniciId"] = kisi.UserId;

                    if (Session["BasvuruSayfasi"] != null)
                    {
                        return Redirect("/Home/Single/" + Session["BasvuruSayfasi"]);
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }

                }
                else
                {
                    return View();

                }
            }
            else
            {
                return View();

            }

        }
        #endregion


    }
}