﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kampus.DAL;

namespace Kampus.Web.Controllers
{
    public class ApiSettingsController : ApiController
    {
        private MyModel db = new MyModel();

        // GET: api/ApiSettings
        public IQueryable<Settings> GetSettings()
        {
            return db.Settings;
        }

        // GET: api/ApiSettings/5
        [ResponseType(typeof(Settings))]
        public IHttpActionResult GetSettings(short id)
        {
            Settings settings = db.Settings.Find(id);
            if (settings == null)
            {
                return NotFound();
            }

            return Ok(settings);
        }

        // PUT: api/ApiSettings/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSettings(short id, Settings settings)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != settings.SettingId)
            {
                return BadRequest();
            }

            db.Entry(settings).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SettingsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiSettings
        [ResponseType(typeof(Settings))]
        public IHttpActionResult PostSettings(Settings settings)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Settings.Add(settings);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SettingsExists(settings.SettingId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = settings.SettingId }, settings);
        }

        // DELETE: api/ApiSettings/5
        [ResponseType(typeof(Settings))]
        public IHttpActionResult DeleteSettings(short id)
        {
            Settings settings = db.Settings.Find(id);
            if (settings == null)
            {
                return NotFound();
            }

            db.Settings.Remove(settings);
            db.SaveChanges();

            return Ok(settings);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SettingsExists(short id)
        {
            return db.Settings.Count(e => e.SettingId == id) > 0;
        }
    }
}