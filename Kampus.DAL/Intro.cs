namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Intro")]
    public partial class Intro
    {
        public byte IntroId { get; set; }

        [StringLength(150)]
        public string Header { get; set; }

        [StringLength(100)]
        public string Button { get; set; }

        [StringLength(200)]
        public string ImageUrl { get; set; }
    }
}
