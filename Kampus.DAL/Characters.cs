namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Characters
    {
        [Key]
        public int CharacterId { get; set; }

        public int? GameId { get; set; }

        [StringLength(100)]
        public string CharacterName { get; set; }

        [StringLength(200)]
        public string CharacterImageURL { get; set; }

        public string CharacterDescription { get; set; }

        public bool? IsActive { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CreatedDate { get; set; }

        public virtual Games Games { get; set; }
    }
}
