namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GameDetail")]
    public partial class GameDetail
    {
        [Key]
        [Column(Order = 0)]
        public byte GameDetailId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string BigHeader { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(80)]
        public string BigText { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string FirstRules { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string SecondRules { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(50)]
        public string ThirdRules { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(50)]
        public string FourthRules { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(50)]
        public string FifthRules { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(50)]
        public string SixthRules { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(20)]
        public string FirstHeader { get; set; }

        [Key]
        [Column(Order = 10)]
        [StringLength(20)]
        public string SecondHeader { get; set; }

        [Key]
        [Column(Order = 11)]
        [StringLength(20)]
        public string ThirdHeader { get; set; }

        [Key]
        [Column(Order = 12)]
        [StringLength(20)]
        public string FourthHeader { get; set; }

        [Key]
        [Column(Order = 13)]
        [StringLength(20)]
        public string FifthHeader { get; set; }

        [Key]
        [Column(Order = 14)]
        [StringLength(20)]
        public string SixthHeader { get; set; }

        [Key]
        [Column(Order = 15)]
        [StringLength(30)]
        public string SecondBigHeader { get; set; }

        [Key]
        [Column(Order = 16)]
        [StringLength(50)]
        public string BackgroundImage { get; set; }

        [Key]
        [Column(Order = 17)]
        [StringLength(20)]
        public string ButtonWhite { get; set; }

        [Key]
        [Column(Order = 18)]
        [StringLength(20)]
        public string ButtonYellow { get; set; }
    }
}
