namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaskOfJoin")]
    public partial class TaskOfJoin
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TaskOfJoinId { get; set; }

        [StringLength(200)]
        public string Header { get; set; }

        [StringLength(200)]
        public string Rule1 { get; set; }

        [StringLength(200)]
        public string Rule2 { get; set; }

        [StringLength(200)]
        public string Rule3 { get; set; }

        [StringLength(200)]
        public string Rule4 { get; set; }

        [StringLength(200)]
        public string Rule5 { get; set; }

        [StringLength(200)]
        public string Rule6 { get; set; }
    }
}
