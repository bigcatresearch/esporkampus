namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Users()
        {
            TeamAdvertisements = new HashSet<TeamAdvertisements>();
            UsersToGames = new HashSet<UsersToGames>();
            UsersToTeams = new HashSet<UsersToTeams>();
        }

        [Key]
        public int UserId { get; set; }

        public int? TeamId { get; set; }

        public int? MatchId { get; set; }

        public int GameId { get; set; }

        public int? UniversityId { get; set; }

        [StringLength(100)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        public int? Role { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CreatedDate { get; set; }

        public bool? IsActive { get; set; }

        public string UserAbout { get; set; }

        public int? Wins { get; set; }

        public int? Loses { get; set; }

        public int? Ties { get; set; }

        [StringLength(200)]
        public string UniversityIdURL { get; set; }

        [StringLength(200)]
        public string UserPhotoURL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeamAdvertisements> TeamAdvertisements { get; set; }

        public virtual Universities Universities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsersToGames> UsersToGames { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsersToTeams> UsersToTeams { get; set; }
    }
}
