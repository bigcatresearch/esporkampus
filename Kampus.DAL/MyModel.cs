namespace Kampus.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MyModel : DbContext
    {
        public MyModel()
            : base("name=MyModel")
        {
        }

        public virtual DbSet<About> About { get; set; }
        public virtual DbSet<Blogs> Blogs { get; set; }
        public virtual DbSet<CalendarOfGames> CalendarOfGames { get; set; }
        public virtual DbSet<Characters> Characters { get; set; }
        public virtual DbSet<GamerAdvertisements> GamerAdvertisements { get; set; }
        public virtual DbSet<Games> Games { get; set; }
        public virtual DbSet<Intro> Intro { get; set; }
        public virtual DbSet<Matches> Matches { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<Referees> Referees { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<TaskOfJoin> TaskOfJoin { get; set; }
        public virtual DbSet<TeamAdvertisements> TeamAdvertisements { get; set; }
        public virtual DbSet<Teams> Teams { get; set; }
        public virtual DbSet<TeamsToGames> TeamsToGames { get; set; }
        public virtual DbSet<UniPosts> UniPosts { get; set; }
        public virtual DbSet<Universities> Universities { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UsersToGames> UsersToGames { get; set; }
        public virtual DbSet<UsersToTeams> UsersToTeams { get; set; }
        public virtual DbSet<GameDetail> GameDetail { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Games>()
                .HasMany(e => e.TeamsToGames)
                .WithRequired(e => e.Games)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Games>()
                .HasMany(e => e.UsersToGames)
                .WithRequired(e => e.Games)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Teams>()
                .HasMany(e => e.TeamsToGames)
                .WithRequired(e => e.Teams)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Users>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.UsersToGames)
                .WithRequired(e => e.Users)
                .WillCascadeOnDelete(false);
        }
    }
}
