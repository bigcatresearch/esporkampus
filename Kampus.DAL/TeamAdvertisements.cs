namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TeamAdvertisements
    {
        [Key]
        public int TeamAd_Id { get; set; }

        public int? GameId { get; set; }

        public int? UserId { get; set; }

        [StringLength(50)]
        public string Tournament1 { get; set; }

        [StringLength(50)]
        public string Tournament2 { get; set; }

        [StringLength(50)]
        public string Tournament3 { get; set; }

        [StringLength(50)]
        public string LolLeague { get; set; }

        [StringLength(50)]
        public string UserRank { get; set; }

        [StringLength(50)]
        public string StreamProfile { get; set; }

        [StringLength(50)]
        public string NickName { get; set; }

        [StringLength(200)]
        public string DiscordURL { get; set; }

        public string Description { get; set; }

        [StringLength(50)]
        public string RoleInGame { get; set; }

        public virtual Games Games { get; set; }

        public virtual Users Users { get; set; }
    }
}
