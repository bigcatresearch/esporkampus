namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UsersToGames
    {
        [Key]
        public int UTG_Id { get; set; }

        public int UserId { get; set; }

        public int GameId { get; set; }

        [StringLength(200)]
        public string StreamProfile { get; set; }

        [StringLength(50)]
        public string NickName { get; set; }

        [StringLength(100)]
        public string RoleInGame { get; set; }

        [StringLength(100)]
        public string WitchName { get; set; }

        [StringLength(50)]
        public string UserServer { get; set; }

        [StringLength(50)]
        public string UserRank { get; set; }

        public bool? HaveTeam { get; set; }

        public bool? TeamWish { get; set; }

        public virtual Games Games { get; set; }

        public virtual Users Users { get; set; }
    }
}
