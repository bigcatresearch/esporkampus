namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Blogs
    {
        [Key]
        public int BlogId { get; set; }

        public int? WriterId { get; set; }

        public int? GameId { get; set; }

        [StringLength(200)]
        public string BlogTitle { get; set; }

        [StringLength(200)]
        public string BlogImageURL { get; set; }

        public string BlogContent { get; set; }

        public bool? IsActive { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CreatedDate { get; set; }

        public bool? Priority { get; set; }

        public virtual Games Games { get; set; }
    }
}
