namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Games
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Games()
        {
            Blogs = new HashSet<Blogs>();
            CalendarOfGames = new HashSet<CalendarOfGames>();
            Characters = new HashSet<Characters>();
            GamerAdvertisements = new HashSet<GamerAdvertisements>();
            Matches = new HashSet<Matches>();
            TeamAdvertisements = new HashSet<TeamAdvertisements>();
            TeamsToGames = new HashSet<TeamsToGames>();
            UsersToGames = new HashSet<UsersToGames>();
        }

        [Key]
        public int GameId { get; set; }

        [StringLength(100)]
        public string GameName { get; set; }

        public string GameDescription { get; set; }

        [StringLength(200)]
        public string GameLogoURL { get; set; }

        public bool? IsActive { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CreatedDate { get; set; }

        public int? UserCount { get; set; }

        public string GameRules { get; set; }

        [StringLength(300)]
        public string FirstPrize { get; set; }

        [StringLength(300)]
        public string SecondPrize { get; set; }

        [StringLength(300)]
        public string ThirdPrize { get; set; }

        public string GameDescriptionExtra { get; set; }

        [StringLength(50)]
        public string GameCategory { get; set; }

        [StringLength(200)]
        public string GameBackground { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blogs> Blogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CalendarOfGames> CalendarOfGames { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Characters> Characters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GamerAdvertisements> GamerAdvertisements { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Matches> Matches { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeamAdvertisements> TeamAdvertisements { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeamsToGames> TeamsToGames { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsersToGames> UsersToGames { get; set; }
    }
}
