namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CalendarOfGames
    {
        [Key]
        public int CalendarOfGameId { get; set; }

        public int? GameId { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(200)]
        public string Date1 { get; set; }

        [StringLength(200)]
        public string Date2 { get; set; }

        [StringLength(200)]
        public string Date3 { get; set; }

        [StringLength(200)]
        public string Date4 { get; set; }

        [StringLength(200)]
        public string Date5 { get; set; }

        [StringLength(200)]
        public string Date6 { get; set; }

        public virtual Games Games { get; set; }
    }
}
