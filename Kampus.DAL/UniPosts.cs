namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UniPosts
    {
        [Key]
        public int PostId { get; set; }

        public int? UserId { get; set; }

        public int? PrePostId { get; set; }

        public string PostContent { get; set; }

        public int? UniversityId { get; set; }

        public bool? IsActive { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CreatedDate { get; set; }
    }
}
