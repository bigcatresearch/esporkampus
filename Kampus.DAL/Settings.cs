namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Settings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short SettingId { get; set; }

        [StringLength(50)]
        public string CompanyName { get; set; }

        [StringLength(200)]
        public string CompanyLogoURL { get; set; }

        public string CompanyDescription { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(200)]
        public string Adress { get; set; }

        public bool? WebSiteIsActive { get; set; }
    }
}
