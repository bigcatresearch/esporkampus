namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("About")]
    public partial class About
    {
        public byte AboutId { get; set; }

        public string AboutUs { get; set; }

        [StringLength(70)]
        public string Text1Header { get; set; }

        public string Text1Body { get; set; }

        [StringLength(70)]
        public string Text2Header { get; set; }

        public string Text2Body { get; set; }

        [StringLength(50)]
        public string Button_text { get; set; }

        [StringLength(150)]
        public string Button_link { get; set; }
    }
}
