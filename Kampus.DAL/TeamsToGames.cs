namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TeamsToGames
    {
        [Key]
        public int TTG_Id { get; set; }

        public int TeamId { get; set; }

        public int GameId { get; set; }

        public virtual Games Games { get; set; }

        public virtual Teams Teams { get; set; }
    }
}
