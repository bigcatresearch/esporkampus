namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Messages
    {
        [Key]
        public int MessageId { get; set; }

        [StringLength(100)]
        public string SenderName { get; set; }

        [StringLength(100)]
        public string SenderEmail { get; set; }

        [StringLength(50)]
        public string SenderIP { get; set; }

        [StringLength(200)]
        public string Subject { get; set; }

        public string MessageContent { get; set; }

        public bool? IsRead { get; set; }

        public bool? IsUser { get; set; }

        [StringLength(10)]
        public string RowBgColor { get; set; }

        [StringLength(200)]
        public string MessagePhotoURL { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? SendingTime { get; set; }
    }
}
