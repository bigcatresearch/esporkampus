namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GamerAdvertisements
    {
        [Key]
        public int GamerAd_Id { get; set; }

        public int? GameId { get; set; }

        public int? TeamId { get; set; }

        [StringLength(50)]
        public string GamerAdExprerience { get; set; }

        public string GamerAdDescription { get; set; }

        [StringLength(200)]
        public string DiscordURL { get; set; }

        [StringLength(50)]
        public string RoleInGame { get; set; }

        public virtual Games Games { get; set; }

        public virtual Teams Teams { get; set; }
    }
}
