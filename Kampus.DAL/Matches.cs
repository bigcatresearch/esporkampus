namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Matches
    {
        [Key]
        public int MatchId { get; set; }

        public int? GameId { get; set; }

        public int? Team1Id { get; set; }

        public int? Team2Id { get; set; }

        public int? RefereeId { get; set; }

        [StringLength(50)]
        public string StartingTime { get; set; }

        [StringLength(100)]
        public string Place { get; set; }

        public bool? IsActive { get; set; }

        public int? Team1Score { get; set; }

        public int? Team2Score { get; set; }

        [StringLength(50)]
        public string WhatAbout { get; set; }

        public int? WinnerId { get; set; }

        public virtual Games Games { get; set; }

        public virtual Referees Referees { get; set; }
    }
}
