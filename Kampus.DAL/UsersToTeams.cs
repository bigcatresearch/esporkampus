namespace Kampus.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UsersToTeams
    {
        [Key]
        public int UTT_id { get; set; }

        public int? TeamId { get; set; }

        public int? UserId { get; set; }

        public bool? IsCaptain { get; set; }

        public virtual Teams Teams { get; set; }

        public virtual Users Users { get; set; }
    }
}
