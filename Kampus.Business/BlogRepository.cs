﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Kampus.DAL;
using System.Threading.Tasks;

namespace Kampus.Business
{
    public class BlogRepository
    {
        Uri Base_URL = new Uri("http://esporkampus.com/api/");

        public IEnumerable<Blogs> GetAllBlogs()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiBlogs/").Result;
                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<Blogs>>().Result;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Blogs GetBlogById(int? id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiBlogs/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<Blogs>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool CreateBlog(Blogs blog)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync("ApiBlogs/", blog).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditBlog(Blogs blog, int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.PutAsJsonAsync("ApiBlogs/" + id, blog);
                // return response.IsSuccessStatusCode;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBlog(int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.DeleteAsync("ApiBlogs/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
    }
}
