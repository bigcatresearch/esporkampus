﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Kampus.DAL;
using System.Threading.Tasks;
namespace Kampus.Business
{
    public class MessageRepository
    {
        Uri Base_URL = new Uri("http://esporkampus.com/api/");

        public IEnumerable<Messages> GetAllMessages()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiMessages/").Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<Messages>>().Result;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Messages GetMessageById(int? id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiMessages/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<Messages>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool CreateMessage(Messages message)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync("ApiMessages/", message).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditMessage(Messages message, int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.PutAsJsonAsync("ApiMessages/" + id, message);
                // return response.IsSuccessStatusCode;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMessage(int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.DeleteAsync("ApiMessages/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
    }
}
