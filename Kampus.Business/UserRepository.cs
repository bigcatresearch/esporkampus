﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Kampus.DAL;
using System.Threading.Tasks;

namespace Kampus.Business
{
    public class UserRepository
    {
        Uri Base_URL = new Uri("http://esporkampus.com/api/");

        public IEnumerable<Users> GetAllUsers()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiUsers/").Result;
                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<Users>>().Result;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Users GetUserById(int? id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiUsers/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<Users>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool CreateUser(Users user)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync("ApiUsers/", user).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditUser(Users user, int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.PutAsJsonAsync("ApiUsers/" + id, user);
                // return response.IsSuccessStatusCode;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteUser(int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.DeleteAsync("ApiUsers/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
    }
}
