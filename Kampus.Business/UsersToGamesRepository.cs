﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Kampus.DAL;
using System.Threading.Tasks;

namespace Kampus.Business
{
    public class UsersToGamesRepository
    {
        Uri Base_URL = new Uri("http://esporkampus.com/api/");

        public IEnumerable<UsersToGames> GetAllUsersToGames()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiUsersToGames/").Result;
                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<UsersToGames>>().Result;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UsersToGames GetUserToGamesById(int? id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiUsersToGames/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<UsersToGames>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool CreateUsersToGames(UsersToGames user)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync("ApiUsersToGames/", user).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteUserToGames(int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.DeleteAsync("ApiUsersToGames/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
    }
}
