﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Kampus.DAL;
using System.Threading.Tasks;

namespace Kampus.Business
{
    public class GameRepository
    {
        Uri Base_URL = new Uri("http://esporkampus.com/api/");

        public IEnumerable<Games> GetAllGames()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiGames/").Result;
                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<Games>>().Result;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Games GetGameById(int? id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("ApiGames/" + id).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<Games>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool CreateGame(Games game)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync("ApiGames/", game).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditGame(Games game, int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.PutAsJsonAsync("ApiGames/" + id, game);
                // return response.IsSuccessStatusCode;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGame(int id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = Base_URL;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.DeleteAsync("ApiGames/" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
    }
}
