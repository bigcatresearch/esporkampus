﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Kampus.Webb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "Games",
               url: "Oyunlar/{id}",
               defaults: new { controller = "Home", action = "Games", id = "" }
               );

            routes.MapRoute(
               name: "UniversityDetail",
               url: "Universite/{id}",
               defaults: new { controller = "Home", action = "UniversityDetail", id = "" }
               );

            routes.MapRoute(
              name: "Universities",
              url: "Universiteler",
              defaults: new { controller = "Home", action = "Universities" }
              );

            routes.MapRoute(
               name: "Application",
               url: "Basvuru",
               defaults: new { controller = "Home", action = "Application" }
               );

            routes.MapRoute(
               name: "Complaint",
               url: "SikayetveOneri",
               defaults: new { controller = "Home", action = "Complaint" }
               );

            routes.MapRoute(
               name: "AboutUs",
               url: "Biz-kimiz",
               defaults: new { controller = "Home", action = "AboutUs" }
               );

            routes.MapRoute(
               name: "Contact",
               url: "Iletisim",
               defaults: new { controller = "Home", action = "Contact" }
               );

            routes.MapRoute(
              name: "Login",
              url: "Giris-yap",
              defaults: new { controller = "Home", action = "Login" }
              );

            routes.MapRoute(
              name: "LoginToPanel",
              url: "PaneleGiris",
              defaults: new { controller = "Admin", action = "LoginToPanel" }
              );

            routes.MapRoute(
              name: "Register",
              url: "Kayit-ol",
              defaults: new { controller = "Home", action = "Register" }
              );

            routes.MapRoute(
              name: "ActiveGames",
              url: "Aktif-oyunlar",
              defaults: new { controller = "Home", action = "ActiveGames" }
              );

            routes.MapRoute(
               name: "Fixture",
               url: "Fikstur",
               defaults: new { controller = "Home", action = "Fixture" }
               );

            routes.MapRoute(
               name: "Blogs",
               url: "Blog",
               defaults: new { controller = "Home", action = "Blogs" }
               );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
