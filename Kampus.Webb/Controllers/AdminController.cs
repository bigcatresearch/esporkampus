﻿using Kampus.Business;
using Kampus.DAL;
using Kampus.Webb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kampus.Webb.Controllers
{
    public class AdminController : Controller
    {
        MyModel db = new MyModel();
        UserRepository userRepository = new UserRepository();
        MessageRepository messageRepository = new MessageRepository();
        GameRepository gameRepository = new GameRepository();
        BlogRepository blogRepository = new BlogRepository();
        ImageUpload imageUpload = new ImageUpload();

        // GET: Index
        #region

        public ActionResult Index()
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.oyuncuSayisi = db.Users.Where(x => x.Role == 4).Count().ToString();
                ViewBag.takimSayisi = db.Teams.Count().ToString();
                ViewBag.macSayisi = db.Matches.Count().ToString();
                ViewBag.universiteSayisi = db.Universities.Count().ToString();
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion

        // BLOG
        #region

        // GET: Blog
        #region
        public ActionResult Blog()
        {
            if (Session["AdminId"] != null)
            {
                return View(db.Blogs.ToList());
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        //GET: BlogEdit
        #region
        public ActionResult BlogEdit(int id)
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.GameList = db.Games.ToList();
                var blog = db.Blogs.FirstOrDefault(x => x.BlogId == id);
                return View(blog);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        //POST: BlogEdit
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult BlogEdit([Bind(Include = "BlogId,WriterId,GameId,IsActive,BlogTitle,BlogImageURL,BlogContent,CreatedDate,Priority")] Blogs blog, HttpPostedFileBase BlogResmi, int OyunId)
        {
            if (ModelState.IsValid)
            {
                ViewBag.GameList = db.Games.ToList();
                if (blog.BlogTitle == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                if (blog.Priority == true)
                {
                    var oncelikliBlog = db.Blogs.FirstOrDefault(x => x.Priority == true);
                    oncelikliBlog.Priority = false;

                    db.SaveChanges();
                }
                else { blog.Priority = false; }

                var br = db.Blogs.FirstOrDefault(x => x.BlogId == blog.BlogId);
                br.GameId = OyunId;
                br.Priority = blog.Priority;
                br.BlogTitle = blog.BlogTitle;
                br.BlogContent = blog.BlogContent;
                br.BlogImageURL = br.BlogImageURL;
                br.IsActive = br.IsActive;
                if (BlogResmi != null)
                { br.BlogImageURL = imageUpload.ImageResize(BlogResmi, 500, 420); }

                blogRepository.EditBlog(br, blog.BlogId);
                return RedirectToAction("Blog");
            }
            return RedirectToAction("Blog");
        }
        #endregion
        // GET: BlogAdd
        #region
        public ActionResult BlogAdd()
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.GameList = db.Games.ToList();
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        //POST: BlogAdd
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult BlogAdd([Bind(Include = "BlogId,WriterId,GameId,IsActive,BlogTitle,BlogImageURL,BlogContent,CreatedDate,Priority")] Blogs blog, HttpPostedFileBase BlogResmi, int OyunId)
        {
            if (ModelState.IsValid)
            {
                int writer = Convert.ToInt32(Session["AdminId"].ToString());
                ViewBag.GamesList = db.Games.ToList();

                if (blog.BlogTitle == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                if (blog.Priority == true)
                {
                    var oncelikliBlog = db.Blogs.FirstOrDefault(x => x.Priority == true);
                    oncelikliBlog.Priority = false;

                    db.SaveChanges();
                }
                else { blog.Priority = false; }
                blog.GameId = OyunId;
                blog.BlogImageURL = imageUpload.ImageResize(BlogResmi, 500, 420);
                blog.IsActive = true;
                blog.WriterId = writer;
                blog.CreatedDate = DateTime.Now;
                blogRepository.CreateBlog(blog);
                return RedirectToAction("Blog");
            }
            return RedirectToAction("Blog");
        }
        #endregion
        //DELETE: Blogs
        #region
        public ActionResult BlogDelete(int id)
        {
            blogRepository.DeleteBlog(id);
            return Redirect("/Admin/Blog/");
        }
        #endregion
        //DURUM: Blogs
        #region
        public ActionResult BlogDurum(int id)
        {
            var br = db.Blogs.FirstOrDefault(x => x.BlogId == id);
            if (br.Priority == false)
            {
                if (br.IsActive == true)
                {
                    br.IsActive = false;
                }
                else
                {
                    br.IsActive = true;
                }
                db.SaveChanges();
            }

            return RedirectToAction("Blog");
        }
        #endregion
        //GET: BlogDetail
        #region
        public ActionResult BlogDetail(int id)
        {
            if (Session["AdminId"] != null)
            {
                var br = db.Blogs.FirstOrDefault(x => x.BlogId == id);
                return View(br);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        #endregion

        // USERS
        #region

        // GET: Users
        #region
        public ActionResult Users()
        {
            if (Session["AdminId"] != null)
            {
                return View(db.Users.Where(x => x.Role != 4).ToList());
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        // GET: UserAdd
        #region
        public ActionResult UserAdd()
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.PageList = db.Universities.ToList();
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        //POST: UserAdd
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserAdd([Bind(Include = "UserId,UserName,NickName,Email,Password,Role,UniversityId")] Users user)
        {
            if (ModelState.IsValid)
            {
                ViewBag.PageList = db.Universities.ToList();

                if (user.UserName == null || user.Email == null || user.Role == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                
                user.IsActive = true;
                user.CreatedDate = DateTime.Now;
                user.Wins = 0;
                user.Ties = 0;
                user.Loses = 0;
                userRepository.CreateUser(user);
                return Redirect("/Admin/Users/");
            }
            return Redirect("/Admin/Users/");
        }
        #endregion

        //GET: UserDetail
        #region
        public ActionResult UserDetail(int? id)
        {
            return View(userRepository.GetUserById(id));
        }
        #endregion
        // GET: UserEdit 
        #region 
        public ActionResult UserEdit(int id)
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.PageList = db.Universities.ToList();
                var br = db.Users.FirstOrDefault(a => a.UserId == id);
                return View(br);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }

        #endregion
        // POST :UserEdit
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserEdit([Bind(Include = "UserId,UserName,NickName,Email,Password,Role,UniversityId")] int id, Users user)
        {
            if (ModelState.IsValid)
            {
                ViewBag.PageList = db.Universities.ToList();
                if (user.UserName == null || user.Email == null || user.Role == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                
                var br = db.Users.FirstOrDefault(a => a.UserId == id);
                br.Role = user.Role;
                br.UserName = user.UserName;
                if (user.Password!=null)
                {
                    br.Password = user.Password;
                }
                br.Email = user.Email;
                br.UniversityId = user.UniversityId;
                db.SaveChanges();
               // userRepository.EditUser(br, id);
                return Redirect("/Admin/Users/");
            }
            return Redirect("/Admin/Users/");
        }
        #endregion

        //DELETE: Users
        #region
        public ActionResult UserDelete(int id)
        {
            var br = db.Users.FirstOrDefault(x=>x.UserId==id);
            db.Users.Remove(br);
            db.SaveChanges();
            return Redirect("/Admin/Users/");
        }
        #endregion

        //DURUM : Users
        #region
        public ActionResult UserDurum(int id)
        {
            var br = db.Users.FirstOrDefault(a => a.UserId == id);
            if (br.IsActive == true)
            {
                br.IsActive = false;
            }
            else
            {
                br.IsActive = true;
            }
            db.SaveChanges();
            return Redirect("/Admin/Users/");
        }
        #endregion
        #endregion

        // GET:Gamers
        #region
        public ActionResult Gamers()
        {
            if (Session["AdminId"] != null)
            {
                var br = db.Users.Where(x => x.Role == 4).ToList();
                return View(br);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        //GET: GamerDetail
        #region
        public ActionResult GamerDetail(int? id)
        {
            if (Session["AdminId"] != null)
            {
                return View(userRepository.GetUserById(id));
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion

        // MESSAGES
        #region
        // GET: Messages
        #region
        public ActionResult Messages()
        {
            if (Session["AdminId"] != null)
            {
                return View(messageRepository.GetAllMessages());
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }

        }
        #endregion
        // GET: MessageDetail
        #region
        public ActionResult MessageDetail(int id)
        {
            if (Session["AdminId"] != null)
            {
                MessageDurum(id);
                return View(messageRepository.GetMessageById(id));
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
           
        }
        #endregion
        // DURUM: Messages
        #region
        public void MessageDurum(int id)
        {
            var br = db.Messages.FirstOrDefault(x => x.MessageId == id);
            if (br.IsRead == false)
            {
                br.IsRead = true;
                br.RowBgColor = "";
                db.SaveChanges();
            }
        }
        #endregion
        // DELETE : Messages
        #region
        public ActionResult MessageDelete(int id)
        {
            var br = db.Messages.FirstOrDefault(x=>x.MessageId==id);
            db.Messages.Remove(br);
            db.SaveChanges();
            return Redirect("/Admin/Messages/");
        }
        #endregion
        #endregion


        // GET: Settings
        public ActionResult Settings()
        {
            if (Session["AdminId"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        // UNIVERSITIES
        #region
        // GET: Universities
        #region
        public ActionResult Universities()
        {
            if (Session["AdminId"] != null)
            {
                 return View(db.Universities.ToList());
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion
        // GET: UniversityAdd
        #region
        public ActionResult UniversityAdd()
        {
            if (Session["AdminId"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }

        }
        #endregion
        //POST: UniversityAdd
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UniversityAdd([Bind(Include = "UniversityId,UniversityName,UniversityScore,UniversityLogoURL")] Universities university, HttpPostedFileBase UniLogo)
        {
            if (ModelState.IsValid)
            {
                if (university.UniversityName == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                university.UniversityLogoURL = imageUpload.ImageResize(UniLogo, 300, 300);
                db.Universities.Add(university);
                db.SaveChanges();
                return RedirectToAction("Universities");
            }
            return RedirectToAction("Universities");
        }
        #endregion


        #endregion

        // GAMES
        #region
        // GET: Games
        #region
        public ActionResult Games()
        {
            if (Session["AdminId"] != null)
            {
                return View(db.Games.ToList());
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
            
        }
        #endregion
        // GET: GameAdd
        #region
        public ActionResult GameAdd()
        {
            if (Session["AdminId"] != null)
            {
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
           
        }
        #endregion
        //POST: GameAdd
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult GameAdd([Bind(Include = "GameId,GameName,GameLogoURL,IsActive,GameDescription,CreatedDate,UserCount, GameRules,FirstPrize,SecondPrize,ThirdPrize,GameCategory,GameDescriptionExtra")] Games game, HttpPostedFileBase GameImage, HttpPostedFileBase backgroundImage)
        {
            if (ModelState.IsValid)
            {

                if (game.GameName == null || GameImage==null || backgroundImage==null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                game.GameBackground = imageUpload.ImageResize(backgroundImage,1300,1300);
                game.GameLogoURL = imageUpload.ImageResize(GameImage, 500, 420);
                game.IsActive = true;
                game.CreatedDate = DateTime.Now;
                game.UserCount = 0;
                gameRepository.CreateGame(game);
                return RedirectToAction("Games");
            }
            return RedirectToAction("Games");
        }
        #endregion

        //GET: GameDetail
        #region
        public ActionResult GameDetail(int? id)
        {
            return View(gameRepository.GetGameById(id));
        }
        #endregion
        // GET: GameEdit 
        #region 
        public ActionResult GameEdit(int id)
        {
            if (Session["AdminId"] != null)
            {
                MyModel db = new MyModel();
                var br = db.Games.FirstOrDefault(a => a.GameId == id);
                return View(br);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }

        #endregion
        // POST :GameEdit
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult GameEdit([Bind(Include = "GameId,GameName,GameLogoURL,IsActive,GameDescription,CreatedDate,UserCount,GameRules,FirstPrize,SecondPrize,ThirdPrize,GameCategory,GameDescriptionExtra")] int id, Games game, HttpPostedFileBase oyunResmi,HttpPostedFileBase backgroundImage)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                if (game.GameName == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }

                var br = db.Games.FirstOrDefault(a => a.GameId == id);
                br.GameCategory = game.GameCategory;
                br.GameRules = game.GameRules;
                br.FirstPrize = game.FirstPrize;
                br.SecondPrize = game.SecondPrize;
                br.ThirdPrize = game.ThirdPrize;
                br.GameDescription = game.GameDescription;
                game.IsActive = br.IsActive;

                if(oyunResmi!=null)
                {
                    br.GameLogoURL = imageUpload.ImageResize(oyunResmi,500, 420);
                }
                if (backgroundImage != null)
                {
                    br.GameBackground = imageUpload.ImageResize(backgroundImage, 1300, 1300);
                }


                db.SaveChanges();
                return RedirectToAction("Games");
            }
            return RedirectToAction("Games");
        }
        #endregion

        //DELETE: Games
        #region
        public ActionResult GameDelete(int id)
        {
            var br = db.Games.FirstOrDefault(x=>x.GameId==id);
            db.Games.Remove(br);
            db.SaveChanges();
            return Redirect("/Admin/Games/");
        }
        #endregion

        //DURUM : Games
        #region
        public ActionResult GameDurum(int id)
        {
            MyModel db = new MyModel();
            var br = db.Games.FirstOrDefault(a => a.GameId == id);
            if (br.IsActive == true)
            {
                br.IsActive = false;
            }
            else
            {
                br.IsActive = true;
            }
            db.SaveChanges();
            return RedirectToAction("Games");
        }
        #endregion
        #endregion

        // ANA SAYFA Dinamik Ogeleri
        #region
        //GET:Intro
        #region
        public ActionResult Intro()
        {
            if (Session["AdminId"] != null)
            {
                var intro = db.Intro.FirstOrDefault(x => x.IntroId == 1);
                return View(intro);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
           
        }
        #endregion

        //POST:Intro
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Intro([Bind(Include = "IntroId,Header,Button,Background")] Intro intro)
        {
            if (ModelState.IsValid)
            {
                var br = db.Intro.FirstOrDefault(x => x.IntroId == 1);
                br.Header = intro.Header;
                br.ImageUrl = intro.ImageUrl;
                br.Button = intro.Button;
                db.SaveChanges();
                return Redirect("/Admin/Intro/");
            }
            return Redirect("/Admin/Intro/");
        }
        #endregion

        //GET:About
        #region
        public ActionResult About()
        {
            if (Session["AdminId"] != null)
            {
                var about = db.About.FirstOrDefault(x => x.AboutId == 1);
                return View(about);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion

        //POST:About
        #region
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult About([Bind(Include = "AboutId,AboutUs,Text1Header,Text1Body,Text2Header,Text2Body,Button_text,Button_link")] About about)
        {
            var br = db.About.FirstOrDefault(x => x.AboutId == 1);
            br.AboutUs = about.AboutUs;
            br.Button_link = about.Button_link;
            br.Button_text = about.Button_text;
            br.Text1Header = about.Text1Header;
            br.Text1Body = about.Text1Body;
            br.Text2Header = about.Text2Header;
            br.Text2Body = about.Text2Body;

            db.SaveChanges();
            return Redirect("/Admin/About/");
        }
        #endregion

        //GET:Terms
        #region
        public ActionResult Terms()
        {
            if (Session["AdminId"] != null)
            {
                var task = db.TaskOfJoin.FirstOrDefault(x => x.TaskOfJoinId == 1);
                return View(task);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion

        //POST:Terms
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Terms([Bind(Include = "TaskOfJoinId,Header,Rule1,Rule2,Rule3,Rule4,Rule5,Rule6")] TaskOfJoin task)
        {
            if (ModelState.IsValid)
            {
                var br = db.TaskOfJoin.FirstOrDefault(x => x.TaskOfJoinId == 1);
                br.Header = task.Header;
                br.Rule1 = task.Rule1;
                br.Rule2 = task.Rule2;
                br.Rule3 = task.Rule3;
                br.Rule4 = task.Rule4;
                br.Rule5 = task.Rule5;
                br.Rule6 = task.Rule6;

                db.SaveChanges();
                return Redirect("/Admin/Terms/");
            }
            return Redirect("/Admin/Terms/");
        }
        #endregion

        //GET:GameCalendar
        #region
        public ActionResult GameCalendar()
        {
            if (Session["AdminId"] != null)
            {
                return View(db.CalendarOfGames.ToList());
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion

        //GET:GameCalendarDetail
        #region
        public ActionResult GameCalendarDetail(int id)
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.GameList = db.Games.ToList();
                var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
                return View(br);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
           
        #endregion

        //GET:GameCalendarAdd
        #region
        public ActionResult GameCalendarAdd()
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.GameList = db.Games.ToList();
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }
        #endregion

        //POST:GameCalendarAdd
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GameCalendarAdd([Bind(Include = "CalendarOfGameId,GameId,IsActive,Date1,Date2,Date3,Date4,Date5,Date6")] CalendarOfGames calendar)
        {
            if (ModelState.IsValid)
            {
                calendar.IsActive = true;
                db.CalendarOfGames.Add(calendar);
                db.SaveChanges();
                return Redirect("/Admin/GameCalendar/");
            }
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion

        //GET:GameCalendarEdit
        #region
        public ActionResult GameCalendarEdit(int id)
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.GameList = db.Games.ToList();
                var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
                return View(br);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }

        }
        #endregion

        //POST:GameCalendarEdit
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GameCalendarEdit([Bind(Include = "CalendarOfGameId,GameId,IsActive,Date1,Date2,Date3,Date4,Date5,Date6")] CalendarOfGames calendar)
        {
            if (ModelState.IsValid)
            {
                var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == calendar.CalendarOfGameId);
                br.GameId = calendar.GameId;
                br.Date1 = calendar.Date1;
                br.Date2 = calendar.Date2;
                br.Date3 = calendar.Date3;
                br.Date4 = calendar.Date4;
                br.Date5 = calendar.Date5;
                br.Date6 = calendar.Date6;

                db.SaveChanges();
                return Redirect("/Admin/GameCalendar/");
            }
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion

        //DURUM:GameCalendarDurum
        #region
        public ActionResult GameCalendarDurum(int id)
        {
            var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
            if (br.IsActive == true)
            {
                br.IsActive = false;
            }
            else
            {
                br.IsActive = true;
            }
            db.SaveChanges();
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion

        //DELETE: GameCalendar
        #region
        public ActionResult GameCalendarDelete(int id)
        {
            var br = db.CalendarOfGames.FirstOrDefault(x => x.CalendarOfGameId == id);
            db.CalendarOfGames.Remove(br);
            db.SaveChanges();
            return Redirect("/Admin/GameCalendar/");
        }
        #endregion
        #endregion

        // MAÇLAR
        #region

        //MATCHES
        #region
        public ActionResult Matches(int id)
        {
            if (Session["AdminId"]!=null)
            {
                var br = db.Matches.Where(x => x.GameId == id).ToList();
                ViewBag.OyunAdi = db.Games.FirstOrDefault(x => x.GameId == id).GameName;
                ViewBag.OyunId = db.Games.FirstOrDefault(x => x.GameId == id).GameId;
                return View(br);
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
            
        }
        #endregion

        //MATCH ADD
        #region
        public ActionResult MatchAdd()
        {
            if (Session["AdminId"] != null)
            {
                ViewBag.Hata = false;
                ViewBag.Mesaj = "";
                ViewBag.GamesList = db.Games.Where(x => x.IsActive == true).ToList();
                return View();
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }
        }

        [HttpPost]
        public ActionResult MatchAdd(Matches match,int oyunId,string turCinsi)
        {
            ViewBag.GamesList = db.Games.Where(x => x.IsActive == true).ToList();
            var turVarMi = db.Matches.Where(x => x.GameId == oyunId).FirstOrDefault(c => c.WhatAbout == turCinsi);
            if (turVarMi==null)
            {
                if (turCinsi == "1. Tur")
                {
                    List<TeamsToGames> takimListe = db.TeamsToGames.Where(x => x.Teams.IsActive == true).Where(x => x.GameId == oyunId).ToList();
                    int l = takimListe.Count;
                    //  int macSayisi = l / 2;
                    var shuffledcards = takimListe.OrderBy(a => Guid.NewGuid()).ToList();
                    for (int i = 0; i < takimListe.Count(); i += 2)
                    {
                        db.Matches.Add(new Matches
                        {
                            Team1Id = shuffledcards[i].TeamId,
                            Team2Id = shuffledcards[i + 1].TeamId,
                            GameId = oyunId,
                            StartingTime = match.StartingTime,
                            WhatAbout = turCinsi,
                            IsActive = true
                        });
                        db.SaveChanges();
                    }

                    return Redirect("/Admin/Matches/" + oyunId);
                }
                else if (turCinsi == "2. Tur")
                {
                    List<TeamsToGames> takimListe = db.TeamsToGames.Where(x => x.Teams.IsActive == true).Where(x => x.GameId == oyunId).ToList();
                    List<Matches> maclar = db.Matches.Where(x => x.IsActive == true).Where(x => x.WhatAbout == "1. Tur").Where(c => c.GameId == oyunId).ToList();
                    List<Teams> takim = new List<Teams>();
                    foreach (var item in maclar)
                    {
                        takim.Add(new Teams
                        {
                            MatchId = item.WinnerId  //TeamId boş geçilemez olduğu için TeamId yerine MatchId kullandım.
                        });
                    }
                    int l = takim.Count;
                    //  int macSayisi = l / 2;
                    var shuffledcards = takim.OrderBy(a => Guid.NewGuid()).ToList();
                    for (int i = 0; i < takim.Count(); i += 2)
                    {
                        db.Matches.Add(new Matches
                        {
                            Team1Id = shuffledcards[i].MatchId,
                            Team2Id = shuffledcards[i + 1].MatchId,
                            GameId = oyunId,
                            StartingTime = match.StartingTime,
                            WhatAbout = turCinsi,
                            IsActive = true
                        });
                        db.SaveChanges();
                    }

                    return Redirect("/Admin/Matches/" + oyunId);
                }
                else if (turCinsi == "3. Tur")
                {
                    List<TeamsToGames> takimListe = db.TeamsToGames.Where(x => x.Teams.IsActive == true).Where(x => x.GameId == oyunId).ToList();
                    List<Matches> maclar = db.Matches.Where(x => x.IsActive == true).Where(x => x.WhatAbout == "2. Tur").Where(c => c.GameId == oyunId).ToList();
                    List<Teams> takim = new List<Teams>();
                    foreach (var item in maclar)
                    {
                        takim.Add(new Teams
                        {
                            MatchId = item.WinnerId  //TeamId boş geçilemez olduğu için TeamId yerine MatchId kullandım.
                        });
                    }
                    int l = takim.Count;
                    //  int macSayisi = l / 2;
                    var shuffledcards = takim.OrderBy(a => Guid.NewGuid()).ToList();
                    for (int i = 0; i < takim.Count(); i += 2)
                    {
                        db.Matches.Add(new Matches
                        {
                            Team1Id = shuffledcards[i].MatchId,
                            Team2Id = shuffledcards[i + 1].MatchId,
                            GameId = oyunId,
                            StartingTime = match.StartingTime,
                            WhatAbout = turCinsi,
                            IsActive = true
                        });
                        db.SaveChanges();
                    }

                    return Redirect("/Admin/Matches/" + oyunId);
                }
                else if (turCinsi == "4. Tur")
                {
                    List<TeamsToGames> takimListe = db.TeamsToGames.Where(x => x.Teams.IsActive == true).Where(x => x.GameId == oyunId).ToList();
                    List<Matches> maclar = db.Matches.Where(x => x.IsActive == true).Where(x => x.WhatAbout == "3. Tur").Where(c => c.GameId == oyunId).ToList();
                    List<Teams> takim = new List<Teams>();
                    foreach (var item in maclar)
                    {
                        takim.Add(new Teams
                        {
                            MatchId = item.WinnerId  //TeamId boş geçilemez olduğu için TeamId yerine MatchId kullandım.
                        });
                    }
                    int l = takim.Count;
                    //  int macSayisi = l / 2;
                    var shuffledcards = takim.OrderBy(a => Guid.NewGuid()).ToList();
                    for (int i = 0; i < takim.Count(); i += 2)
                    {
                        db.Matches.Add(new Matches
                        {
                            Team1Id = shuffledcards[i].MatchId,
                            Team2Id = shuffledcards[i + 1].MatchId,
                            GameId = oyunId,
                            StartingTime = match.StartingTime,
                            WhatAbout = turCinsi,
                            IsActive = true
                        });
                        db.SaveChanges();
                    }
                    return Redirect("/Admin/Matches/" + oyunId);
                }
                else if (turCinsi == "5. Tur")
                {
                    List<TeamsToGames> takimListe = db.TeamsToGames.Where(x => x.Teams.IsActive == true).Where(x => x.GameId == oyunId).ToList();
                    List<Matches> maclar = db.Matches.Where(x => x.IsActive == true).Where(x => x.WhatAbout == "4. Tur").Where(c => c.GameId == oyunId).ToList();
                    List<Teams> takim = new List<Teams>();
                    foreach (var item in maclar)
                    {
                        takim.Add(new Teams
                        {
                            MatchId = item.WinnerId  //TeamId boş geçilemez olduğu için TeamId yerine MatchId kullandım.
                        });
                    }
                    int l = takim.Count;
                    //  int macSayisi = l / 2;
                    var shuffledcards = takim.OrderBy(a => Guid.NewGuid()).ToList();
                    for (int i = 0; i < takim.Count(); i += 2)
                    {
                        db.Matches.Add(new Matches
                        {
                            Team1Id = shuffledcards[i].MatchId,
                            Team2Id = shuffledcards[i + 1].MatchId,
                            GameId = oyunId,
                            StartingTime = match.StartingTime,
                            WhatAbout = turCinsi,
                            IsActive = true
                        });
                        db.SaveChanges();
                    }
                    return Redirect("/Admin/Matches/" + oyunId);
                }
                else { return View(); }
                
            }
            else
            {
                ViewBag.Hata = true;
                ViewBag.Mesaj = "Eklemek istediğiniz tur zaten mevcut!";
                return View();
            }
            
        }
        #endregion

        //MATCH DURUM
        #region
        public ActionResult MatchDurum(int id)
        {
            var br = db.Matches.FirstOrDefault(x => x.MatchId == id);
            if (br.IsActive == false)
            {
                br.IsActive = true;
                db.SaveChanges();
            }
            else
            {
                br.IsActive = false;
                db.SaveChanges();
            }
            return RedirectToAction("Matches");
        }
        #endregion

        //MATCH DELETE
        #region
        public ActionResult MatchDelete(int id)
        {
            var br = db.Matches.FirstOrDefault(x => x.MatchId == id);
            db.Matches.Remove(br);
            db.SaveChanges();
            return RedirectToAction("Matches");
        }
        #endregion

       
        //MACLARI GETIR
        [HttpPost]
        public JsonResult MaclariGetir(string turnuva)
        {
            MyModel db = new MyModel();
            var maclar = db.Matches.Where(x => x.WhatAbout == turnuva).ToList(); ;
            return Json(maclar, JsonRequestBehavior.AllowGet);
        }

        // SKOR EKLE
        #region
         public ActionResult MatchUpdate(int id, string takim1, string takim2)
        {
            var br = db.Matches.FirstOrDefault(x=>x.MatchId==id);
            int takim1Skor= Convert.ToInt32(takim1);
            int takim2Skor = Convert.ToInt32(takim2);
            if (takim1Skor>takim2Skor)
            {
                br.WinnerId = br.Team1Id;
            }
            else
            {
                br.WinnerId = br.Team2Id;
            }
            br.Team1Score =takim1Skor;
            br.Team2Score = takim2Skor;
            db.SaveChanges();

            return Redirect("/Admin/Matches/"+br.GameId);
        }
        #endregion
        public ActionResult Tournaments()
        {
            if (Session["AdminId"] != null)
            {
                return View(db.Games.Where(x => x.IsActive == true).ToList());
            }
            else
            {
                return Redirect("/Admin/LoginToPanel");
            }

        }
        #endregion

        //LOGIN
        #region
        public ActionResult LoginToPanel()
        {
            ViewBag.Hata = false;
            ViewBag.Mesaj = "";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginToPanel(string email, string password)
        {
            MyModel db = new MyModel();
            var kisi = db.Users.FirstOrDefault(x => x.Email == email);
            if (kisi != null)
            {
                if (kisi.Password == password && kisi.IsActive == true && kisi.Role==0)
                {
                    Session["AdminId"] = kisi.UserId;
                    return Redirect("/Admin/Index");
                }
                else
                {
                    ViewBag.Hata = true;
                    ViewBag.Mesaj = "E-Posta veya Parola Hatalı!";
                    return View();
                }
            }
            else
            {
                ViewBag.Hata = true;
                ViewBag.Mesaj = "E-Posta veya Parola Hatalı!";
                return View();
            }

        }

        public ActionResult Logout()
        {
            Session["AdminId"] = null;
            return Redirect("/Admin/LoginToPanel");
        }

        #endregion



       
    }
}