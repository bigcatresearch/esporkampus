﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kampus.DAL;

namespace Kampus.Webb.Controllers
{
    public class ApiGamesController : ApiController
    {
        private MyModel db = new MyModel();

        // GET: api/ApiGames
        public IQueryable<Games> GetGames()
        {
            return db.Games;
        }

        // GET: api/ApiGames/5
        [ResponseType(typeof(Games))]
        public IHttpActionResult GetGames(int id)
        {
            Games games = db.Games.Find(id);
            if (games == null)
            {
                return NotFound();
            }

            return Ok(games);
        }

        // PUT: api/ApiGames/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGames(int id, Games games)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != games.GameId)
            {
                return BadRequest();
            }

            db.Entry(games).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GamesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiGames
        [ResponseType(typeof(Games))]
        public IHttpActionResult PostGames(Games games)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Games.Add(games);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = games.GameId }, games);
        }

        // DELETE: api/ApiGames/5
        [ResponseType(typeof(Games))]
        public IHttpActionResult DeleteGames(int id)
        {
            Games games = db.Games.Find(id);
            if (games == null)
            {
                return NotFound();
            }

            db.Games.Remove(games);
            db.SaveChanges();

            return Ok(games);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GamesExists(int id)
        {
            return db.Games.Count(e => e.GameId == id) > 0;
        }
    }
}