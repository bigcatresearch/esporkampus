﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kampus.DAL;

namespace Kampus.Webb.Controllers
{
    public class ApiUniversitiesController : ApiController
    {
        private MyModel db = new MyModel();

        // GET: api/ApiUniversities
        public IQueryable<Universities> GetUniversities()
        {
            return db.Universities;
        }

        // GET: api/ApiUniversities/5
        [ResponseType(typeof(Universities))]
        public IHttpActionResult GetUniversities(int id)
        {
            Universities universities = db.Universities.Find(id);
            if (universities == null)
            {
                return NotFound();
            }

            return Ok(universities);
        }

        // PUT: api/ApiUniversities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUniversities(int id, Universities universities)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != universities.UniversityId)
            {
                return BadRequest();
            }

            db.Entry(universities).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UniversitiesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiUniversities
        [ResponseType(typeof(Universities))]
        public IHttpActionResult PostUniversities(Universities universities)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Universities.Add(universities);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = universities.UniversityId }, universities);
        }

        // DELETE: api/ApiUniversities/5
        [ResponseType(typeof(Universities))]
        public IHttpActionResult DeleteUniversities(int id)
        {
            Universities universities = db.Universities.Find(id);
            if (universities == null)
            {
                return NotFound();
            }

            db.Universities.Remove(universities);
            db.SaveChanges();

            return Ok(universities);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UniversitiesExists(int id)
        {
            return db.Universities.Count(e => e.UniversityId == id) > 0;
        }
    }
}