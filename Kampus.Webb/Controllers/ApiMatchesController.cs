﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kampus.DAL;

namespace Kampus.Webb.Controllers
{
    public class ApiMatchesController : ApiController
    {
        private MyModel db = new MyModel();

        // GET: api/ApiMatches
        public IQueryable<Matches> GetMatches()
        {
            return db.Matches;
        }

        // GET: api/ApiMatches/5
        [ResponseType(typeof(Matches))]
        public IHttpActionResult GetMatches(int id)
        {
            Matches matches = db.Matches.Find(id);
            if (matches == null)
            {
                return NotFound();
            }

            return Ok(matches);
        }

        // PUT: api/ApiMatches/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMatches(int id, Matches matches)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != matches.MatchId)
            {
                return BadRequest();
            }

            db.Entry(matches).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MatchesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApiMatches
        [ResponseType(typeof(Matches))]
        public IHttpActionResult PostMatches(Matches matches)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Matches.Add(matches);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = matches.MatchId }, matches);
        }

        // DELETE: api/ApiMatches/5
        [ResponseType(typeof(Matches))]
        public IHttpActionResult DeleteMatches(int id)
        {
            Matches matches = db.Matches.Find(id);
            if (matches == null)
            {
                return NotFound();
            }

            db.Matches.Remove(matches);
            db.SaveChanges();

            return Ok(matches);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MatchesExists(int id)
        {
            return db.Matches.Count(e => e.MatchId == id) > 0;
        }
    }
}