﻿using Kampus.Business;
using Kampus.DAL;
using Kampus.Webb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Kampus.Webb.Controllers
{
    public class HomeController : Controller
    {
        ImageUpload imageUpload = new ImageUpload();
        UserRepository userRepository = new UserRepository();
        MessageRepository messageRepository = new MessageRepository();
        TeamRepository teamRepository = new TeamRepository();
        UsersToGamesRepository usersToGamesRepository = new UsersToGamesRepository();

        public ActionResult Index()
        {
            MyModel db = new MyModel();
            var intro = db.Intro.FirstOrDefault(x => x.IntroId == 1);
            ViewBag.IntroBaslik = intro.Header;
            ViewBag.ButtonLink = intro.Button;

            var about = db.About.FirstOrDefault(x => x.AboutId == 1);
            ViewBag.BizKimiz = about.AboutUs;
            ViewBag.Text1Baslik = about.Text1Header;
            ViewBag.Text1Icerik = about.Text1Body;
            ViewBag.Text2Baslik = about.Text2Header;
            ViewBag.Text2Icerik = about.Text2Body;

            var terms = db.TaskOfJoin.FirstOrDefault(x => x.TaskOfJoinId == 1);
            ViewBag.Header = terms.Header;
            ViewBag.Rule1 = terms.Rule1;
            ViewBag.Rule2 = terms.Rule2;
            ViewBag.Rule3 = terms.Rule3;
            ViewBag.Rule4 = terms.Rule4;
            ViewBag.Rule5 = terms.Rule5;
            ViewBag.Rule6 = terms.Rule6;


            var OncelikliBlog = db.Blogs.FirstOrDefault(x => x.Priority == true);
            ViewBag.BlogBaslik = OncelikliBlog.BlogTitle;
            ViewBag.BlogId = OncelikliBlog.BlogId;
            if (OncelikliBlog.BlogContent.Count() > 250)
            { ViewBag.BlogIcerik = OncelikliBlog.BlogContent.Substring(0, 250); }
            else { ViewBag.BlogIcerik = OncelikliBlog.BlogContent; }
            ViewBag.BlogTarih = OncelikliBlog.CreatedDate;
            ViewBag.BlogResim = OncelikliBlog.BlogImageURL;
            return View();
        }

        //public ActionResult Index()
        //{
        //    MyModel db = new MyModel();

        //    //ViewBag.Calendar = db.CalendarOfGames.Where(x => x.IsActive == true);

        //    var intro = db.Intro.FirstOrDefault(x => x.IntroId == 1);
        //    ViewBag.IntroBaslik = intro.Header;
        //    ViewBag.ButtonLink = intro.Button;

        //    var about = db.About.FirstOrDefault(x => x.AboutId == 1);
        //    ViewBag.BizKimiz = about.AboutUs;
        //    ViewBag.Text1Baslik = about.Text1Header;
        //    ViewBag.Text1Icerik = about.Text1Body;
        //    ViewBag.Text2Baslik = about.Text2Header;
        //    ViewBag.Text2Icerik = about.Text2Body;

        //    var terms = db.TaskOfJoin.FirstOrDefault(x => x.TaskOfJoinId == 1);
        //    ViewBag.Header = terms.Header;
        //    ViewBag.Rule1 = terms.Rule1;
        //    ViewBag.Rule2 = terms.Rule2;
        //    ViewBag.Rule3 = terms.Rule3;
        //    ViewBag.Rule4 = terms.Rule4;
        //    ViewBag.Rule5 = terms.Rule5;
        //    ViewBag.Rule6 = terms.Rule6;


        //    var OncelikliBlog = db.Blogs.FirstOrDefault(x => x.Priority == true);
        //    ViewBag.BlogBaslik = OncelikliBlog.BlogTitle;
        //    ViewBag.BlogId = OncelikliBlog.BlogId;
        //    if (OncelikliBlog.BlogContent.Count() > 250)
        //    { ViewBag.BlogIcerik = OncelikliBlog.BlogContent.Substring(0, 250); }
        //    else { ViewBag.BlogIcerik = OncelikliBlog.BlogContent; }
        //    ViewBag.BlogTarih = OncelikliBlog.CreatedDate;
        //    ViewBag.BlogResim = OncelikliBlog.BlogImageURL;
        //    return View();
        //}

        public ActionResult Blogs()
        {
            MyModel db = new MyModel();
            return View(db.Blogs.Where(x => x.IsActive == true).ToList());
        }

        public ActionResult BlogDetail(int id)
        {
            MyModel db = new MyModel();
            var br = db.Blogs.FirstOrDefault(x => x.BlogId == id);
            return View(br);
        }

        public ActionResult GameOffer(string OyunTeklifi)
        {
            if (Session["KullaniciId"]==null)
            {
                return RedirectToAction("Login");
            }

            else
            {
                MyModel db = new MyModel();
                int userID = Convert.ToInt32(Session["KullaniciId"].ToString());
                var user = db.Users.FirstOrDefault(x => x.UserId == userID);

                var ipAddress = string.Empty;
                if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"] != null && System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"].Length != 0)
                {
                    ipAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"];
                }
                else if (System.Web.HttpContext.Current.Request.UserHostAddress.Length != 0)
                {
                    ipAddress = System.Web.HttpContext.Current.Request.UserHostName;
                }
                db.Messages.Add(new Messages
                {
                    SenderName = user.UserName,
                    SenderEmail = user.Email,
                    IsRead = false,
                    MessageContent = OyunTeklifi,
                    SenderIP = ipAddress.ToString(),
                    SendingTime = DateTime.Now,
                    Subject = "Yeni oyun talebi",
                    RowBgColor = "#F5F5DC"
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
        }

        // TAKIM BAŞVURUSU
        #region
        public ActionResult Team(int id)
        {
            MyModel db = new MyModel();
            if (Session["KullaniciId"] != null)
            {
                var userId = Convert.ToInt32(Session["KullaniciId"].ToString());
                var uniId = db.Users.FirstOrDefault(x=>x.UserId==userId).UniversityId;
                ViewBag.Universite = db.Universities.FirstOrDefault(x => x.UniversityId == uniId).UniversityName;
                ViewBag.Game = db.Games.FirstOrDefault(x => x.GameId == id).GameName;
                ViewBag.TeamList = db.Teams.ToList();
                ViewBag.Arkaplan = db.Games.FirstOrDefault(x => x.GameId == id).GameBackground;
                Session["oyunId"] = id;
                return View();
            }
            else
            {
                Session["LoginYonlendirme"] = "/Home/Team/" + id;
                Session["TakimBasvuruBilgi"] = "Takım Başvurusu için önce bireysel kayıt yapmalısınız.";
                return Redirect("/Home/Single/"+id);
            }

        }
        // TAKIM EKLEME KISMI
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Team([Bind(Include = "TeamId,UserId,MatchId,TeamDetailId,UniversityId,GamesId,TeamName,About,LogoImageURL,TeamCount,IsActive,CreatedDate,Wins,Loses,Ties,GameId")] Teams team, HttpPostedFileBase TeamLogo)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {
                int kId = Convert.ToInt32(Session["KullaniciId"].ToString());
                var kullanici = db.Users.FirstOrDefault(x => x.UserId == kId);
                GuidTekrar:
                var uid = Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "").Substring(0,5);
                var guidControl = db.Teams.FirstOrDefault(x => x.TeamGuid == uid);
                if(guidControl!=null)
                { goto GuidTekrar; }

                var oyunId = Convert.ToInt32(Session["oyunId"].ToString());
                team.UniversityId = kullanici.UniversityId;
                team.TeamGuid = uid;
                team.LogoImageURL = imageUpload.ImageResize(TeamLogo, 500, 420);
                team.GameId = oyunId;
                team.IsActive = true;
                team.CreatedDate = DateTime.Now;
                team.GamerWish = false;
                team.Wins = 0;
                team.Ties = 0;
                team.Loses = 0;
                teamRepository.CreateTeam(team);

                db.UsersToTeams.Add(new UsersToTeams
                {
                    UserId = kullanici.UserId,
                    TeamId = db.Teams.Max(x => x.TeamId),
                    IsCaptain=true
                });
                db.SaveChanges();
                db.TeamsToGames.Add(new TeamsToGames
                {
                    TeamId = db.Teams.Max(x => x.TeamId),
                    GameId = oyunId
                });
                db.SaveChanges();
                return Redirect("/Home/TeamPage/" + db.Teams.Max(x => x.TeamId));
            }
            return Redirect("/Home/TeamPage/" + db.Teams.Max(x => x.TeamId));
        }
        #endregion

        // BİREYSEL BAŞVURU
        #region


        public ActionResult Single(int id)
        {
            MyModel db = new MyModel();
            ViewBag.Oyun = db.Games.FirstOrDefault(x => x.GameId == id).GameName;
            Session["OyunId"] = id;
            if (Session["KullaniciId"] == null)
            {
                ViewBag.KisiselBilgiler = true;
            }
            else
            {
                ViewBag.KisiselBilgiler = false;
            }
            if (Session["TakimBasvuruBilgi"]!=null)
            {
                ViewBag.Hata = "true";
                ViewBag.Mesaj = Session["TakimBasvuruBilgi"].ToString();
                Session["TakimBasvuruBilgi"] = null;
            }
            else
            {
                ViewBag.Hata = "";
                ViewBag.Mesaj = "";

            }
            ViewBag.Arkaplan = db.Games.FirstOrDefault(x => x.GameId == id).GameBackground;
            ViewBag.UniversityList = db.Universities.ToList();
            ViewBag.GamesList = db.Games.ToList();
            ViewBag.TeamList = db.Teams.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Single(Users user, string RoleinGame, string witchName, string UserRank, string UserServer, string StreamProfile, bool haveAteam, string oyunId, string teamCode, HttpPostedFileBase UniKimlik, int? UniversityId, HttpPostedFileBase Fotograf)
        {
            if (ModelState.IsValid)
            {
                oyunId = Session["OyunId"].ToString();
                MyModel db = new MyModel();
                ViewBag.UniversityList = db.Universities.ToList();
                ViewBag.GamesList = db.Games.ToList();
                
                if (Session["KullaniciId"]==null)
                {
                    var kullaniciVarMi = db.Users.FirstOrDefault(x => x.Email == user.Email);
                    if (kullaniciVarMi == null)
                    {
                        if (haveAteam == null || teamCode=="")
                        {
                            haveAteam = false;
                        }
                        db.Users.Add(new Users
                        {
                            UserName = user.UserName,
                            Email = user.Email,
                            Password = user.Password,
                            UserPhotoURL = imageUpload.ImageResize(Fotograf, 500, 500),
                            CreatedDate = DateTime.Now,
                            Address = user.Address,
                            BirthDate = user.BirthDate,
                            IsActive = true,
                            Loses = 0,
                            Wins = 0,
                            Ties = 0,
                            Phone = user.Phone,
                            Role = 4,
                            UniversityId = UniversityId,
                            UniversityIdURL = imageUpload.ImageResize(UniKimlik, 400, 200)
                        });
                        db.SaveChanges();
                        Session["KullaniciId"] = db.Users.Max(x => x.UserId);
                        UsersToGames utg = new UsersToGames();
                        utg.GameId = Convert.ToInt32(oyunId);
                        utg.UserId = db.Users.Max(x => x.UserId);
                        utg.WitchName = witchName;
                        utg.StreamProfile = StreamProfile;
                        utg.UserServer = UserServer;
                        utg.RoleInGame = RoleinGame;
                        utg.HaveTeam = haveAteam;
                        utg.TeamWish = false;
                        utg.UserRank = UserRank;

                        MessagePackage message = new MessagePackage();
                        MailSending sendMail = new MailSending();

                        message.Subject = "Ekampüs'e Hoşgeldiniz!";
                        message.ToEmail = user.Email;
                        message.Body = "Sn. <b>" + user.UserName + "</b> Espor Kampüs'e katıldığınız için teşekkürler.<br><br>  -Espor Kampüs'e ekibi";
                        sendMail.Send(message);

                        if (haveAteam == true && teamCode != null)
                        {
                            var team = db.Teams.FirstOrDefault(x => x.TeamGuid == teamCode);
                            if (team != null)
                            {
                                db.UsersToGames.Add(utg);
                                db.SaveChanges();
                                if (team.UniversityId==utg.Users.UniversityId)
                                {
                                    db.UsersToTeams.Add(new UsersToTeams
                                    {
                                        TeamId = team.TeamId,
                                        UserId = db.Users.Max(x => x.UserId)
                                    });
                                    db.SaveChanges();
                                    return Redirect("/Home/TeamPage/" + team.TeamId);
                                }
                                else
                                {
                                    ViewBag.Hata = "true";
                                    ViewBag.Mesaj = "Sadece aynı üniversitedeki kişiler takım olabilir.";
                                    return Redirect("/Home/JoinToTeam/" + team.TeamId);
                                }
                               
                               
                            }
                            else
                            {
                                ViewBag.Hata = "true";
                                ViewBag.Mesaj = "Takım kodu hatalı!";
                                return View();
                            }
                        }
                        else
                        {
                            db.UsersToGames.Add(utg);
                            db.SaveChanges();
                            return Redirect("/Home/Games/" + oyunId);
                        }
                    }
                    else
                    {
                        ViewBag.Hata = "true";
                        ViewBag.Mesaj = "Kullanıcı zaten var! Lütfen giriş yapınız.";
                        return View();
                    }
                }
                else
                {
                    int oyun = Convert.ToInt32(oyunId);
                    int kullaniciID = Convert.ToInt32(Session["KullaniciId"]);
                    var kullanici = db.Users.FirstOrDefault(x=>x.UserId==kullaniciID);
                    var zatenOyuncu = db.UsersToGames.Where(x => x.GameId == oyun).Where(z => z.UserId == kullaniciID).Count();
                    if (zatenOyuncu==0)
                    {
                        UsersToGames utg = new UsersToGames();
                        utg.GameId = Convert.ToInt32(oyunId);
                        utg.UserId = kullaniciID;
                        utg.WitchName = witchName;
                        utg.StreamProfile = StreamProfile;
                        utg.UserServer = UserServer;
                        utg.RoleInGame = RoleinGame;
                        utg.HaveTeam = haveAteam;
                        utg.TeamWish = false;
                        utg.UserRank = UserRank;
                        if (haveAteam == true && teamCode != null)
                        {
                            var team = db.Teams.FirstOrDefault(x => x.TeamGuid == teamCode);
                            if (team != null)
                            {
                                var takimdaMi = db.UsersToTeams.Where(x => x.TeamId == team.TeamId).Where(z => z.UserId == kullaniciID).Count();
                                if (takimdaMi==0)
                                {
                                    db.UsersToGames.Add(utg);
                                    db.SaveChanges();

                                    if (team.UniversityId==utg.Users.UniversityId)
                                    {
                                        db.UsersToTeams.Add(new UsersToTeams
                                        {
                                            TeamId = team.TeamId,
                                            UserId = kullaniciID
                                        });
                                        db.SaveChanges();
                                        return Redirect("/Home/TeamPage/" + team.TeamId);
                                    }
                                    else
                                    {
                                        ViewBag.Hata = "true";
                                        ViewBag.Mesaj = "Sadece aynı üniversitedeki kişiler takım olabilir.";
                                        return Redirect("/Home/JoinToTeam/" + team.TeamId);
                                    }
                                }
                                else
                                {
                                    ViewBag.Hata = "true";
                                    ViewBag.Mesaj = "Takıma zaten üyesiniz!";
                                    return View();
                                }
                            }
                            else
                            {
                                ViewBag.Hata = "true";
                                ViewBag.Mesaj = "Takım kodu hatalı!";
                                return View();
                            }
                        }
                        else
                        {
                            db.UsersToGames.Add(utg);
                            db.SaveChanges();
                            return Redirect("/Home/Games/" + oyunId);
                        }
                    }
                    else
                    {
                        ViewBag.Hata = "true";
                        ViewBag.Mesaj = "Bu oyuna zaten üyesiniz!";
                        return View();
                    }

                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        #endregion

        // UNİVERSİTE 
        #region

        public ActionResult Universities()
        {
            MyModel db = new MyModel();
            return View(db.Universities.ToList());
        }

        public ActionResult UniversityDetail(int? id)
        {
            if (id != null)
            {
                Session["UniID"] = id;
                MyModel db = new MyModel();
                var uni = db.Universities.FirstOrDefault(x => x.UniversityId == id);
                ViewBag.Teams = db.Teams.Where(x => x.UniversityId == id).Where(x => x.IsActive == true).ToList();
                ViewBag.YorumEkle = false;

                var usersWhoWishTeam = db.TeamAdvertisements.Where(x => x.Users.IsActive == true).Where(x => x.Users.UniversityId == id);
                
                var teamsWhoWishGamer = db.GamerAdvertisements.Where(x => x.Teams.IsActive == true).Where(x => x.Teams.UniversityId == id);
                ViewBag.usersWhoWishTeam = usersWhoWishTeam.ToList();
                ViewBag.teamsWhoWishGamer = teamsWhoWishGamer.ToList();

                ViewBag.GameAds = db.GamerAdvertisements.ToList();
                ViewBag.TeamAds = db.TeamAdvertisements.ToList();

                if (Session["KullaniciId"] != null)
                {
                    string kId = Session["KullaniciId"].ToString();
                    int kidd = Convert.ToInt32(kId);
                    ViewBag.KullaniciID = kidd;

                    ViewBag.GamesList = db.Games.ToList();
                    ViewBag.TeamList = db.UsersToTeams.Where(x => x.UserId == kidd).ToList();
                }
                return View(uni);
            }
            else
            {
                return RedirectToAction("Universities");
            }

        }

        public ActionResult GamerAdvertisementAdd()
        {
            if(Session["KullaniciId"]!=null)
            {
                MyModel db = new MyModel();
                int kid = Convert.ToInt32(Session["KullaniciId"].ToString());
                var user = db.Users.FirstOrDefault(x => x.UserId == kid);
                int uniID = Convert.ToInt32(Session["UniID"].ToString());
                if (user.UniversityId == uniID)
                {
                    ViewBag.GamesList = db.Games.ToList();
                    ViewBag.TeamList = db.UsersToTeams.Where(x => x.UserId == kid).ToList();
                    return View();
                }
                else
                {
                    return Redirect("/Home/Universities/");
                }  
                
            }
            else
            {
                return RedirectToAction("Login");
            }
            
        }

        [HttpPost]
        public ActionResult GamerAdvertisementAdd(GamerAdvertisements gamerAds, int teamId, int gameId, string gamerExperience, string gamerDescription, string DiscordAdress,string gamerRole)
        {
            MyModel db = new MyModel();
            db.GamerAdvertisements.Add(new GamerAdvertisements
            {
                TeamId = teamId,
                GameId = gameId,
                RoleInGame=gamerRole,
                DiscordURL = DiscordAdress,
                GamerAdExprerience = gamerExperience,
                GamerAdDescription = gamerDescription
            });
            db.SaveChanges();
            return RedirectToAction("Universities");
        }

        public ActionResult TeamAdvertisementAdd()
        {
            if (Session["KullaniciId"] != null)
            {
                MyModel db = new MyModel();
                int id = Convert.ToInt32(Session["KullaniciId"].ToString());
                var user = db.Users.FirstOrDefault(x => x.UserId == id);
                int uniID = Convert.ToInt32(Session["UniID"].ToString());
                if (user.UniversityId==uniID)
                {
                    ViewBag.GamesList = db.Games.ToList();
                    return View();
                }
                else
                {
                    return Redirect("/Home/Universities/");
                }
               
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult TeamAdvertisementAdd(int gameId, string turnuva1, string turnuva2, string turnuva3, string LolLigi, string rutbe, string streamProfili, string nick, string dicord, string Description,string gamerRole)
        {
            MyModel db = new MyModel();
            string kId = Session["KullaniciId"].ToString();
            int kidd = Convert.ToInt32(kId);
            var kullanici = db.Users.FirstOrDefault(x => x.UserId == kidd);

            db.TeamAdvertisements.Add(new TeamAdvertisements
            {
                UserId = kullanici.UserId,
                GameId = gameId,
                Tournament1 = turnuva1,
                Tournament2 = turnuva2,
                RoleInGame= gamerRole,
                Tournament3 = turnuva3,
                LolLeague = LolLigi,
                StreamProfile = streamProfili,
                UserRank = rutbe,
                NickName = nick,
                DiscordURL = dicord,
                Description = Description
            });
            db.SaveChanges();
            return RedirectToAction("Universities");
        }

        public ActionResult GamerAdSingle(int id)
        {
            MyModel db = new MyModel();
            var br = db.TeamAdvertisements.FirstOrDefault(x => x.TeamAd_Id == id);
            return View(br);
        }

        public ActionResult TeamAdSingle(int id)
        {
            MyModel db = new MyModel();
            var br = db.GamerAdvertisements.FirstOrDefault(x => x.GamerAd_Id == id);
            return View(br);
        }

        public ActionResult UniversityAdd()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UniversityAdd([Bind(Include = "UniversityId,UniversityName")]Universities Universite, string UniversiteAdi)
        {
            Universite.UniversityName = UniversiteAdi;
            Universite.UniversityScore = 0;
            using (HttpClient client = new HttpClient())
            {
                var data = JsonConvert.SerializeObject(Universite);
                HttpContent content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
                var returnResult = await client.PostAsync("http://esporkampus.com/api/ApiUniversities", content);
            }
            return View();
        }

        public ActionResult TeamWish(int id, int gameID)
        {
            MyModel db = new MyModel();
            var user = db.UsersToGames.Where(x => x.UserId == id).FirstOrDefault(x => x.GameId == gameID);
            user.TeamWish = true;
            db.SaveChanges();
            return Redirect("/Home/Universities/");
        }


        #endregion

        // POST: Message
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "MessageId,SenderName,SenderEmail,SenderIP,Subject,IsRead,IsUser,RowBgColor,SendingTime")] Messages message, string Mesaj, string AdSoyad, string Eposta, string Konu, HttpPostedFileBase resim)
        {
            if (ModelState.IsValid)
            {

                if (AdSoyad == "" || Eposta == "" || Mesaj == "")
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                else
                {
                    var ipAddress = string.Empty;
                    if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                    {
                        ipAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    else if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"] != null && System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"].Length != 0)
                    {
                        ipAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"];
                    }
                    else if (System.Web.HttpContext.Current.Request.UserHostAddress.Length != 0)
                    {
                        ipAddress = System.Web.HttpContext.Current.Request.UserHostName;
                    }

                    MyModel db = new MyModel();
                    var IsUser = db.Users.FirstOrDefault(x => x.Email == Eposta);
                    if (IsUser != null)
                    {
                        message.IsUser = true;
                    }
                    else
                    {
                        message.IsUser = false;
                    }

                    if (resim!=null)
                    {
                        message.MessagePhotoURL = imageUpload.ImageResize(resim,500, 500);
                    }
                    message.SenderEmail = Eposta;
                    message.SenderName = AdSoyad;
                    message.Subject = Konu;
                    message.SenderIP = ipAddress.ToString();
                    message.IsRead = false;
                    message.SendingTime = DateTime.Now;
                    message.RowBgColor = "#F5F5DC";
                    message.MessageContent = Mesaj;
                    messageRepository.CreateMessage(message);
                }
               
            }
            return Redirect("/Home/Index/");
        }
        #endregion

        public ActionResult TeamPage(int id)
        {
            MyModel db = new MyModel();
            var team = db.Teams.FirstOrDefault(x => x.TeamId == id);
            if(team!=null)
            { 
                if (Session["KullaniciId"] != null)
                {
                    int kId = Convert.ToInt32(Session["KullaniciId"].ToString());
                    var kullanici = db.Users.FirstOrDefault(x => x.UserId == kId);
                    var takimdaMi = db.UsersToTeams.Where(x => x.TeamId == id).FirstOrDefault(c=>c.Users.UserId==kId);
                    var takim = db.UsersToTeams.FirstOrDefault(x => x.TeamId == id);
                    if (takim!=null)
                    {
                        if (takimdaMi!=null)
                        {
                            ViewBag.KatilGoster = false;
                            var kaptanMi = db.UsersToTeams.Where(c => c.TeamId == takim.TeamId).FirstOrDefault(x => x.UserId == kId).IsCaptain;
                            if (kaptanMi == true)
                            {
                                ViewBag.OyuncuEkleGoster = true;
                            }
                            else
                            {
                                ViewBag.OyuncuEkleGoster = false;
                            }
                        }
                        else
                        {
                            ViewBag.KatilGoster = true;
                        }
                       
                        return View(team);
                    }
                    else
                    {
                        return RedirectToAction("Login");
                    } 
                }
                else
                {
                   return RedirectToAction("Login");
                }
               
            }
            else
            {
                return RedirectToAction("Error");
            }
        }

        public ActionResult JoinToTeam()
        {
            if (Session["KullaniciId"]!=null || Session["KullaniciId"].ToString()=="")
            {
                ViewBag.YanlisKod = "";
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
           
        }

        [HttpPost]
        public ActionResult JoinToTeam(string TeamGuid)
        {
            MyModel db = new MyModel();
            var team = db.Teams.FirstOrDefault(x=>x.TeamGuid==TeamGuid);
            int kId = Convert.ToInt32(Session["KullaniciId"].ToString());
            var kullanici = db.Users.FirstOrDefault(x=>x.UserId==kId);
            if (team!=null)
            {
                if (kullanici.UniversityId==team.UniversityId)
                {
                    db.UsersToTeams.Add(new UsersToTeams
                    {
                        TeamId = team.TeamId,
                        UserId = kId
                    });
                    db.SaveChanges();
                    return Redirect("/Home/TeamPage/" + team.TeamId);
                }
                else
                {
                    ViewBag.Hata = "true";
                    ViewBag.Mesaj = "Takım ile sizin üniversiteniz uyuşmamaktadır!";
                    return View();
                }
               
            }
            else
            {
                ViewBag.Hata = "true";
                ViewBag.Mesaj = "Hatalı Kod Girdiniz";
                return View();
            }

        }

        public ActionResult Games(int id)
        {
            MyModel db = new MyModel();

            var game = db.Games.FirstOrDefault(x => x.GameId == id);
            ViewBag.Teams = db.TeamsToGames.Where(x => x.GameId == id).ToList();
            ViewBag.Blogs = db.Blogs.Where(x => x.IsActive == true).Where(x => x.GameId == id).ToList();
            return View(game);
        }

        // REGISTER
        #region

        public ActionResult Register()
        {
            if (Session["KullaniciId"] == null || Session["KullaniciId"].ToString() == "")
            {
                MyModel db = new MyModel();
                ViewBag.UniversityList = db.Universities.ToList();
                ViewBag.Hata = false;
                ViewBag.Mesaj = "";
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }   
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register([Bind(Include = "UserId,TeamId,GameId,MatchId,UniversityId,UserName,NickName,Email,Password,Role,BirthDate,Phone,Address,CreatedDate,IsActive,UserAbout,Wins,Loses,Ties,StreamProfile")] Users user, HttpPostedFileBase Fotograf)
        {
            MyModel db = new MyModel();
            if (ModelState.IsValid)
            {

                ViewBag.UniversiteList = db.Universities.ToList();
                ViewBag.OyunList = db.Games.ToList();
                ViewBag.TakımList = db.Teams.ToList();

                if (user.UserName == null || user.Email == null || Fotograf == null)
                {
                    ViewBag.Hata = true;
                    ViewBag.Mesaj = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                else
                {
                    var kayitliMi = db.Users.FirstOrDefault(x=>x.Email==user.Email);
                    if(kayitliMi==null)
                    {
                        db.Users.Add(new Users {
                            UserPhotoURL = imageUpload.ImageResize(Fotograf, 500, 500),
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            Ties = 0,
                            Wins = 0,
                            Loses = 0,
                            Role = 4
                          });
                        db.SaveChanges();
                       
                        MessagePackage message = new MessagePackage();
                        MailSending sendMail = new MailSending();

                        message.Subject = "Ekampüs'e Hoşgeldiniz!";
                        message.ToEmail = user.Email;
                        message.Body = "Sn. <b>" + user.UserName + "</b> Espor Kampüs'e katıldığınız için teşekkürler.<br><br>  -Espor Kampüs'e ekibi";
                        sendMail.Send(message);
                        return Redirect("/Home/Login/");
                    }
                    else
                    {
                        ViewBag.Hata = true;
                        ViewBag.Mesaj = "E-Postanız zaten kayıtlı!";
                        return View();
                    }
                    
                }

            }
            return Redirect("/Home/Login/");
        }
        #endregion
        // LOGIN
        #region

        public ActionResult Login()
        {
            if (Session["KullaniciId"] == null || Session["KullaniciId"].ToString() == "")
            {
                ViewBag.Hata = false;
                ViewBag.Mesaj = "";
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
           
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string email, string password)
        {
            MyModel db = new MyModel();
            var kisi = db.Users.FirstOrDefault(x => x.Email == email);
            if (kisi != null)
            {
                if (kisi.Password == password && kisi.IsActive == true)
                {
                    Session["KullaniciId"] = kisi.UserId;

                    if (Session["LoginYonlendirme"] != null)
                    {
                        return Redirect(Session["LoginYonlendirme"].ToString());
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }

                }
                else
                {
                    ViewBag.Hata = true;
                    ViewBag.Mesaj = "E-Posta veya Parola Hatalı!";
                    return View();
                }
            }
            else
            {
                ViewBag.Hata = true;
                ViewBag.Mesaj = "Giriş yapabilmek için öncelikle kayıt olun!";
                return View();
            }

        }
        #endregion

        public ActionResult SignOut()
        {
            Session["KullaniciId"] = null;
            return RedirectToAction("Index");
        }

        public ActionResult Timeline()
        {
            MyModel db = new MyModel();

            ViewBag.Calendar = db.CalendarOfGames.Where(x => x.IsActive == true).ToList();
            return PartialView();
        }

        public ActionResult GamerAdd(int id)
        {
            Session["TakimId"] = id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GamerAdd(string OyuncuMail)
        {
            MessagePackage message = new MessagePackage();
            MailSending sendMail = new MailSending();
            MyModel db = new MyModel();
            int takimID = Convert.ToInt32(Session["TakimId"].ToString());
            var takim = db.Teams.FirstOrDefault(x => x.TeamId == takimID);

            message.Subject = "Takım Üyeliği";
            message.ToEmail = OyuncuMail;
            message.Body = "Esporkampus'ten <b>"+takim.TeamName+"</b> isimli takım, sizi takıma dahil etmek istiyor.<br> Takıma katılmak için aşağıdaki linke tıklayınız.<br>  <br> <a href=http://esporkampus.com/Home/Single/" + takim.GameId + "><input type=submit value=Katıl></input></a><br> <br> Takım Kodunuz: " + takim.TeamGuid+ "<br><br>  EsporKampus'e kayıt olmayı unutmayınız!!!";
            sendMail.Send(message);
            return Redirect("/Home/TeamPage/"+takimID);
        }

        public ActionResult UniPostAdd(string postContent)
        {
            MyModel db = new MyModel();
            int uniId = Convert.ToInt32(Session["UniId"].ToString());
            if (Session["KullaniciId"]!=null)
            {
                int kId = Convert.ToInt32(Session["KullaniciId"].ToString());
                var kullanici = db.Users.FirstOrDefault(x => x.UserId == kId);

                db.UniPosts.Add(new UniPosts
                {
                    UserId = kId,
                    PostContent = postContent,
                    UniversityId = uniId,
                    IsActive = true,
                    CreatedDate = DateTime.Now
                });
                db.SaveChanges();
            }
            else
            {
                Redirect("/Home/Login/");
            }
            return Redirect("/Home/UniversityDetail/"+uniId);
        }

        [HttpPost]
        public ActionResult UniSubPostAdd(string postID, string subPostContent)
        {
            MyModel db = new MyModel();

            int uniId = Convert.ToInt32(Session["UniId"].ToString());
            if (Session["KullaniciId"] != null)
            {
                int kId = Convert.ToInt32(Session["KullaniciId"].ToString());
                var kullanici = db.Users.FirstOrDefault(x => x.UserId == kId);

                db.UniPosts.Add(new UniPosts
                {
                    UserId = kId,
                    PostContent = subPostContent,
                    UniversityId = uniId,
                    PrePostId=Convert.ToInt32(postID),
                    IsActive = true,
                    CreatedDate = DateTime.Now
                });
                db.SaveChanges();
                return Redirect("/Home/UniversityDetail/"+uniId);
            }
            else
            {
              return Redirect("/Home/Login/");
            }
        }

        [HttpPost]
        public JsonResult OyuncuyuGetir(int id)
        {
            MyModel db = new MyModel();
            int colSayi = 3;
            var utt = db.UsersToTeams.Where(x=>x.TeamId==id);
            List<Users> Oyuncular = new List<Users>();
            if (utt.Count()==1)
            {
                colSayi = 12;
            }
            else if (utt.Count()==2)
            {
                colSayi = 6;
            }
            else if (utt.Count()==3)
            {
                colSayi = 4;
            }
            foreach (var item in utt)
            {
                string foto = "/Content/landing/img/arif/players.png";
                if (item.Users.UserPhotoURL!=null)
                {
                    foto = item.Users.UserPhotoURL;
                }
                Oyuncular.Add(new Users
                {
                    UserId=item.Users.UserId,
                    UserName=item.Users.UserName,
                    UserPhotoURL=foto,
                    UniversityId=item.Users.UniversityId,
                    Role=item.Users.Role,
                    MatchId=colSayi
                });
            }
           
            return Json(Oyuncular, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TakiminOyunu(int id)
        {
            MyModel db = new MyModel();
            var oyunid = db.Teams.FirstOrDefault(x => x.TeamId == id).GameId;
            var oyun = db.Games.FirstOrDefault(x => x.GameId == oyunid).GameName;
            return Json(oyun, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Fixture()
        {
            MyModel db = new MyModel();
            return View(db.Games.ToList());
        }

        public ActionResult Contact()
        {
            ViewBag.BosVeri = "";
            return View();
        }

        [HttpPost]
        public ActionResult Contact(Messages message, string Mesaj, string AdSoyad, string Eposta, string Konu, HttpPostedFileBase resim)
        {
            if (AdSoyad == "" || Eposta == "" || Mesaj == "")
            {
                ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                return View();
            }
            else
            {
                return Index(message, Mesaj, AdSoyad, Eposta, Konu,resim);
            }
           
        }

        public ActionResult AboutUs()
        {
            MyModel db = new MyModel();
            var about = db.About.FirstOrDefault(x => x.AboutId == 1);
            ViewBag.BizKimiz = about.AboutUs;
            return View();
        }

        public ActionResult Application()
        {
            return View();
        }

        public ActionResult ActiveGames()
        {
            MyModel db = new MyModel();
            return View(db.Games.Where(x=>x.IsActive==true).ToList());
        }

        public ActionResult Complaint()
        {
            ViewBag.BosVeri = "";
            return View();
        }

        [HttpPost]
        public ActionResult Complaint(Messages message, string Mesaj, string AdSoyad, string Eposta, string Konu, HttpPostedFileBase resim)
        {
            if (AdSoyad == "" || Eposta == "" || Mesaj == "" || Konu== "--Lütfen Seçiniz--")
            {
                ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                return View();
            }
            else
            {
                return Index(message, Mesaj, AdSoyad, Eposta, Konu, resim);
            }
        }

        public ActionResult UserProfile()
        {
            if (Session["KullaniciId"]!=null)
            {
                MyModel db = new MyModel();
                int id = Convert.ToInt32(Session["KullaniciId"].ToString());
                ViewBag.UniversityList = db.Universities.ToList();
                return View(db.Users.FirstOrDefault(x => x.UserId == id));
            }
            else
            {
                return Redirect("/Home/Login");
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserProfile(Users user, int id, HttpPostedFileBase UniKimlik, HttpPostedFileBase Fotograf)
        {
            MyModel db = new MyModel();
            ViewBag.UniversityList = db.Universities.ToList();
            var br = db.Users.FirstOrDefault(x => x.UserId == id);
            br.UserName = user.UserName;
            br.Password = user.Password;
            br.Phone = user.Phone;
            br.Email = user.Email;
            br.UniversityId = user.UniversityId;
            br.BirthDate = user.BirthDate;
            if (UniKimlik!=null)
            {
                br.UniversityIdURL = imageUpload.ImageResize(UniKimlik,500,500);
            }
            if (Fotograf!=null)
            {
                br.UserPhotoURL = imageUpload.ImageResize(Fotograf,500,500);
            }
            db.SaveChanges();
            return View(br);
        }
    }

}
