﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kampus.Webb.Models
{
    public class MessagePackage
    {
        public MessagePackage()
        {

        }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int? CompanyId { get; set; }
        public string LogoPath { get; set; }
        public string TrackingId { get; set; }
        public Guid TrackingGuid { get; set; }
    }
}